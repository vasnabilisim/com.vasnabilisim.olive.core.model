package com.vasnabilisim.olive.core.model.xmlmodel;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xmlmodel.XmlModel;

/**
 * @author Menderes Fatih GUVEN
 */
public enum XmlModelType {

	SqlModel,
	Model;

	private static Map<XmlModelType, XmlModel> modelMap = new TreeMap<XmlModelType, XmlModel>();

	public static Object putRef(String name, Object ref) {
		return XmlModel.getDecoder().putRef(name, ref);
	}
	
	public XmlModel getModel() throws BaseException {
		XmlModel model = modelMap.get(this);
		if (model != null)
			return model;
		InputStream in = XmlModelType.class.getResourceAsStream(this.name() + ".xmlmodel");
		try {
			model = XmlModel.parse(in);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
		modelMap.put(this, model);
		return model;
	}

	/**
	 * Reads object from given string.
	 * 
	 * @param text
	 * @return
	 * @throws BaseException 
	 */
	public Object read(String text) throws BaseException {
		try {
			return getModel().decode(text);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Reads object from given file.
	 * 
	 * @param file
	 * @return
	 * @throws BaseException
	 */
	public Object read(File file) throws BaseException {
		try {
			return getModel().decode(file);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Reads object from given input stream.
	 * 
	 * @param in
	 * @return
	 * @throws BaseException
	 */
	public Object read(InputStream in) throws BaseException {
		try {
			return getModel().decode(in);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Reads object from given reader.
	 * 
	 * @param reader
	 * @return
	 * @throws BaseException
	 */
	public Object read(Reader reader, XmlModelType type) throws BaseException {
		try {
			return getModel().decode(reader);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Returns string representation of given object.
	 * 
	 * @param object
	 * @return
	 * @throws BaseException
	 */
	public String toString(Object object) throws BaseException {
		try {
			return getModel().encodeToString(object);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Writes given object to given file.
	 * 
	 * @param object
	 * @param file
	 * @throws BaseException
	 */
	public void write(Object object, File file) throws BaseException {
		try {
			getModel().encodeToFile(object, file);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Writes given object to given output stream.
	 * 
	 * @param object
	 * @param out
	 * @param type
	 * @throws BaseException
	 */
	public void write(Object object, OutputStream out) throws BaseException {
		try {
			getModel().encodeToOutputStream(object, out);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}

	/**
	 * Writes given object to given writer.
	 * 
	 * @param object
	 * @param writer
	 * @throws BaseException
	 */
	public void write(Object object, Writer writer) throws BaseException {
		try {
			getModel().encodeToWriter(object, writer);
		} catch (XmlException e) {
			throw new ErrorException(e);
		}
	}
}
