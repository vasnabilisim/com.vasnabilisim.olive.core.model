package com.vasnabilisim.olive.core.model;

import java.util.LinkedList;

/**
 * @author Menderes Fatih GÜVEN
 */
public class FieldList extends LinkedList<Field> {
	private static final long serialVersionUID = 4955085200285614191L;

	public FieldList() {
		super();
	}

	public FieldList(FieldList source) {
		super(source);
	}
}
