package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.util.TreeMap;

/**
 * Parameter map; used to initialize report model.
 * @author Menderes Fatih GUVEN
 */
public class Parameters extends TreeMap<String, Object> implements Serializable {
	private static final long serialVersionUID = -4456035089308696517L;

	
	public static final Parameters Empty = new Parameters();

}
