package com.vasnabilisim.olive.core.model;

import java.io.Serializable;

import com.vasnabilisim.util.Cloneable;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjValue implements Serializable, Cloneable<ObjValue>, java.lang.Cloneable {
	private static final long serialVersionUID = -263214809492262955L;
	
	Object value;
	ObjType type;

	public ObjValue(Object value, ObjType type) {
		this.value = value;
		this.type = type;
	}
	
	@Override
	public String toString() {
		return type + "(" + value + ")";
	}

	public Object getValue() {
		return value;
	}
	
	public ObjType getType() {
		return type;
	}
	
	@Override
	public ObjValue cloneObject() {
		return new ObjValue(this.value, this.type);
	}
	
	@Override
	public ObjValue clone() {
		return cloneObject();
	}
}
