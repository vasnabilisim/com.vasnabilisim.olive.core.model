package com.vasnabilisim.olive.core.model;

import com.vasnabilisim.core.BaseException;

/**
 * Model interface. Provides schema and data. 
 * @author Menderes Fatih GUVEN
 */
public interface Model {

	/**
	 * Returns the name of the report model.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Returns the info of the report model.
	 * 
	 * @return
	 */
	ModelInfo getInfo();

	/**
	 * Returns parameter schema.
	 * 
	 * @return
	 */
	ObjInfoList getParameterInfoList();

	/**
	 * Returns variable schema
	 * 
	 * @return
	 */
	ObjInfo getRootObjInfo();

	/**
	 * Initializes the report model.
	 * 
	 * @param params
	 * @throws BaseException
	 */
	void initialize(Parameters params) throws BaseException;
	
	Obj getRootObj();
}
