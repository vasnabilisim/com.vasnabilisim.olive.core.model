package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.vasnabilisim.util.BooleanFormat;

/**
 * @author Menderes Fatih GUVEN
 */
public class Format implements Serializable, Cloneable, com.vasnabilisim.util.Cloneable<Format> {
	private static final long serialVersionUID = -889958420059097825L;
	
	private ObjType type;
	private String pattern;
	private java.text.Format format;
	
	public Format() {
	}

	public Format(ObjType type, String pattern) {
		this.type = type;
		setPattern(pattern);
	}

	public Format(Format source) {
		this.type = source.type;
		this.pattern = source.pattern;
		this.format = source.format;
	}
	
	@Override
	public Format cloneObject() {
		return new Format(this);
	}
	
	@Override
	public Format clone() {
		return cloneObject();
	}

	public ObjType getType() {
		return type;
	}
	
	public void setType(ObjType type) {
		this.type = type;
	}
	
	public String getPattern() {
		return pattern;
	}
	
	public void setPattern(String pattern) {
		format = null;
		this.pattern = pattern;
		if(pattern != null)
			switch (type) {
			case DateTime:
			case Date:
			case Time:
				format = new SimpleDateFormat(pattern);
				break;
			case Integer:
			case Decimal:
				format = new DecimalFormat(pattern);
				break;
			case Boolean:
				format = new BooleanFormat(pattern);
				break;
			default:
				break;
			}
	}
	
	public java.text.Format getFormat() {
		return format;
	}
	
	@Override
	public String toString() {
		return type.name() + "," + pattern;
	}
	
	public static Format valueOf(String stringValue) {
		if(stringValue == null)
			return null;
		int index = stringValue.indexOf(',');
		if(index < 0)
			return new Format(ObjType.Other, null);
		String typeText = stringValue.substring(0, index);
		String pattern = index == stringValue.length()-1 ? null : stringValue.substring(index + 1);
		ObjType type;
		try {
			type = Enum.valueOf(ObjType.class, typeText);
		} catch (Exception e) {
			type = ObjType.Other;
		}
		return new Format(type, pattern);
	}
}
