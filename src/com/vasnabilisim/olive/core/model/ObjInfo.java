package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.vasnabilisim.util.Cloneable;
import com.vasnabilisim.util.CloneableArrayList;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjInfo implements Comparable<ObjInfo>, Cloneable<ObjInfo>, java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = -3899985110063123500L;

	ObjInfo parent;
	
	String typeName;
	String fieldName;
	ObjType type;
	String stringValue;
	Object value;
	int index;
	Boolean exclude = Boolean.FALSE;
	
	CloneableArrayList<ObjInfo> childs = null;
	
	public ObjInfo() {
		this(10, null, 0, ObjType.Other);
	}
	
	public ObjInfo(int size) {
		this(size, null, 0, ObjType.Other);
	}
	
	public ObjInfo(int size, String fieldName) {
		this(size, fieldName, 0, ObjType.Other);
	}
	
	public ObjInfo(int size, String fieldName, ObjType type) {
		this(size, fieldName, 0, type);
	}
	
	public ObjInfo(int size, String fieldName, int index, ObjType type) {
		childs = new CloneableArrayList<>(size);
		this.typeName = type.name();
		this.fieldName = fieldName;
		this.index = index;
		this.type = type;
	}
	
	ObjInfo(String typeName, String fieldName, int index, ObjType type, CloneableArrayList<ObjInfo> childs) {
		this.childs = childs;
		this.typeName = typeName;
		this.fieldName = fieldName;
		this.index = index;
		this.type = type;
	}
	
	public ObjInfo(ObjInfo source) {
		this.parent = null;
		this.typeName = source.typeName;
		this.fieldName = source.fieldName;
		this.type = source.type;
		this.stringValue = source.stringValue;
		this.value = source.value;
		this.index = source.index;
		this.exclude = source.exclude;
		this.childs = new CloneableArrayList<ObjInfo>(source.childs.size());
		for(ObjInfo childObjInfo : source.childs) {
			ObjInfo cloneChildObjInfo = new ObjInfo(childObjInfo);
			cloneChildObjInfo.parent = this;
			this.childs.add(cloneChildObjInfo);
		}
	}
	
	@Override
	protected Object clone() {
		return cloneObject();
	}
	
	@Override
	public ObjInfo cloneObject() {
		return new ObjInfo(this);
	}
	
	public ObjInfo toObjInfo(ObjType type) {
		return new ObjInfo(typeName, fieldName, index, type, childs);
	}
	
	@Override
	public String toString() {
		return "(" + typeName + ", " + fieldName + ", "  + type + ", " + index + ")";
	}
	
	public ObjInfo getParent() {
		return parent;
	}
	
	public String getTypeName() {
		return typeName == null ? fieldName : typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	public String getFieldName() {
		return fieldName == null ? typeName : fieldName;
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}

	public Object getValue() {
		return getValue(false);
	}
	
	public Object getValue(boolean nullEnabled) {
		if(value != null)
			return value;
		if(nullEnabled && stringValue == null)
			return null;
		if(stringValue != null) {
			if(type != null)
				value = type.fromStringToObject(stringValue);
		}
		else if(type != null) {
			if(type == ObjType.Object) {
				Obj defaultObj = new Obj(this);
				for(ObjInfo childObjInfo : childs)
					defaultObj.addValue(childObjInfo.getValue());
				value = defaultObj;
			}
			else if(type == ObjType.List) {
				Obj defaultObj = new Obj(this.toObjInfo(ObjType.Object));
				for(ObjInfo childObjInfo : childs)
					defaultObj.addValue(childObjInfo.getValue());
				Obj defaultList = new Obj(this, 1);
				defaultList.addValue(defaultObj);
				value = defaultList;
			}
			else
				value = type.getDefaultObject();
		}
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public String getStringValue() {
		return stringValue == null ? String.valueOf(value) : stringValue;
	}
	
	public void setStringValue(String defaultStringValue) {
		this.stringValue = defaultStringValue;
	}
	
	public ObjType getType() {
		return type;
	}
	
	public void setType(ObjType type) {
		this.type = type;
	}
	
	public Boolean getExclude() {
		return exclude == null ? Boolean.FALSE : exclude;
	}
	
	public void setExclude(Boolean exclude) {
		this.exclude = exclude;
	}
	
	@Override
	public int compareTo(ObjInfo other) {
		return Integer.compare(this.index, other.index);
	}
	
	public int getChildCount() {
		return childs.size();
	}

	public ArrayList<ObjInfo> getChilds() {
		return childs;
	}

	public ObjInfo getChild(int index) {
		return childs.get(index);
	}
	
	public void addChild(ObjInfo objInfo) {
		objInfo.parent = this;
		objInfo.setIndex(childs.size());
		childs.add(objInfo);
	}

	public ObjInfo getChild(String childName) {
		for(ObjInfo childResultInfo : childs)
			if(childResultInfo.fieldName.equals(childName))
				return childResultInfo;
		return null;
	}

	public boolean hasChild(String childName) {
		for(ObjInfo childResultInfo : childs)
			if(childResultInfo.fieldName.equals(childName))
				return true;
		return false;
	}

	public void setChild(int index, ObjInfo objInfo) {
		while(index >= childs.size())
			childs.add(null);
		childs.set(index, objInfo);
	}
	
	public ObjInfo solve(String statement) {
		StringTokenizer tokenizer = new StringTokenizer(statement, ".", false);
		String token = null;
		ObjInfo objInfo = this;
		while(tokenizer.hasMoreTokens() && objInfo != null) {
			token = tokenizer.nextToken();
			objInfo = objInfo.getChild(token);
		}
		return objInfo;
	}
}
