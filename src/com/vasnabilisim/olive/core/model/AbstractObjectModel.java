package com.vasnabilisim.olive.core.model;


/**
 * Base class of object models.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractObjectModel implements ObjectModel {

	protected ObjectModelInfo modelInfo =null;

	/**
	 * Info constructor.
	 * 
	 * @param modelInfo
	 */
	public AbstractObjectModel(ObjectModelInfo modelInfo) {
		this.modelInfo = modelInfo;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.Model#getInfo()
	 */
	@Override
	public ObjectModelInfo getInfo() {
		return modelInfo;
	}
	
	@Override
	public String getName() {
		return modelInfo.getName();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}
}
