package com.vasnabilisim.olive.core.model;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.statement.BufferStatement;
import com.vasnabilisim.olive.core.model.statement.EquationStatement;
import com.vasnabilisim.olive.core.model.statement.FieldStatement;
import com.vasnabilisim.olive.core.model.statement.Node;
import com.vasnabilisim.olive.core.model.statement.ParameterStatement;
import com.vasnabilisim.olive.core.model.statement.Statement;
import com.vasnabilisim.olive.core.model.statement.StringStatement;
import com.vasnabilisim.olive.core.model.statement.VariableStatement;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 *
 */
public abstract class AbstractStatementSolver implements StatementSolver {

	/**
	 * Default constructor.
	 */
	public AbstractStatementSolver() {
		super();
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.StatementSolver#solveFieldStatement(com.vasnabilisim.olive.core.model.statement.FieldStatement)
	 */
	@Override
	public Object solveFieldStatement(FieldStatement fieldStatement) throws BaseException {
		if(fieldStatement == null)
			return null;
		return solveStatement(fieldStatement.getStatement());
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.StatementSolver#solveStatement(com.vasnabilisim.olive.core.model.statement.Statement)
	 */
	@Override
	public Object solveStatement(Statement statement) throws BaseException {
		if (statement == null)
			return null;
		if (statement instanceof StringStatement) {
			StringStatement stringStatement = (StringStatement) statement;
			return solveStringStatement(stringStatement);
		}
		if (statement instanceof VariableStatement) {
			VariableStatement variableStatement = (VariableStatement) statement;
			return solveVariableStatement(variableStatement);
		}
		if (statement instanceof ParameterStatement) {
			ParameterStatement parameterStatement = (ParameterStatement) statement;
			return solveParameterStatement(parameterStatement);
		}
		if (statement instanceof EquationStatement) {
			EquationStatement equationStatement = (EquationStatement) statement;
			return solveEquationStatement(equationStatement);
		}
		if (statement instanceof BufferStatement) {
			BufferStatement bufferStatement = (BufferStatement) statement;
			return solveBufferStatement(bufferStatement);
		}
		return null;
	}

	/**
	 * Returns the value represented by given string statement.
	 * 
	 * @param statement
	 * @return
	 */
	public Object solveStringStatement(StringStatement statement) {
		if(statement == null)
			return null;
		return statement.getText();
	}
	
	/**
	 * Returns the value represented by given variable statement.
	 * 
	 * @param statement
	 * @return
	 * @throws BaseException
	 */
	public abstract Object solveVariableStatement(VariableStatement statement) throws BaseException;

	/**
	 * Returns the value represented by given parameter statement.
	 * 
	 * @param statement
	 * @return
	 * @throws BaseException
	 */
	public Object solveParameterStatement(ParameterStatement statement) throws BaseException {
		if(statement == null || getModel() == null)
			return null;
		String parameterName = statement.getText();
		if(StringUtil.isEmpty(parameterName))
			return null;
		for(ObjInfo parameterInfo : getModel().getParameterInfoList()) {
			if(parameterName.equalsIgnoreCase(parameterInfo.getFieldName()))
				return parameterInfo.getValue();
		}
		String message = String.format("Unable to find parameter %s.", parameterName);
		throw new ErrorException(message);
	}

	/**
	 * Calculates and returns the value represented by given equation statement.
	 * 
	 * @param statement
	 * @return
	 * @throws BaseException
	 */
	public Object solveEquationStatement(EquationStatement statement) throws BaseException {
		if(statement == null)
			return null;
		Node rootNode = statement.getRootNode();
		if(rootNode == null)
			return null;
		return rootNode.evaluate(this);
	}

	/**
	 * Evaluates and returns the value represented by given buffer statement.
	 * 
	 * @param statement
	 * @return
	 * @throws BaseException
	 */
	public Object solveBufferStatement(BufferStatement statement) throws BaseException {
		if(statement == null || statement.getStatementList() == null)
			return null;
		StringBuilder builder = new StringBuilder(200);
		Object obj = null;
		for(Statement subStatement : statement.getStatementList()) {
			obj = solveStatement(subStatement);
			if(obj != null)
				//TODO formating may be needed.
				builder.append(obj.toString());
		}
		return builder.toString();
	}
}
