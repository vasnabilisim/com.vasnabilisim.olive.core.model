package com.vasnabilisim.olive.core.model;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.sql.SqlModel;
import com.vasnabilisim.olive.core.model.sql.SqlStatementSolver;

/**
 * @author Menderes Fatih GUVEN
 */
public class StatementSolverFactory {

	private static final StatementSolverFactory instance = new StatementSolverFactory();

	public static StatementSolverFactory getInstance() {
		return instance;
	}

	private StatementSolverFactory() {
	}

	public StatementSolver create(Model model) throws BaseException {
		if (model == null)
			throw new IllegalArgumentException("Null report model.");
		if (model instanceof ObjectModel)
			return new ObjectStatementSolver((ObjectModel) model);
		if (model instanceof SqlModel)
			return new SqlStatementSolver((SqlModel) model);

		String message = String.format("Unsupported model type: %s.", model.getClass().getName());
		throw new ErrorException(message);
	}

}
