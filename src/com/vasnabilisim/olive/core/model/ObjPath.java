package com.vasnabilisim.olive.core.model;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjPath implements Iterable<Obj> {

	LinkedList<Obj> path;
	
	public ObjPath() {
		path = new LinkedList<>();
	}

	public ObjPath(Obj...objs) {
		path = new LinkedList<>();
		for(Obj obj : objs) {
			assert obj != null;
			path.addLast(obj);
		}
	}

	public Obj getFirst() {
		return path.getFirst();
	}
	
	public void addFirst(Obj obj) {
		assert obj != null;
		path.addFirst(obj);
	}
	
	public Obj removeFirst() {
		return path.removeFirst();
	}

	public Obj getLast() {
		return path.getLast();
	}
	
	public void addLast(Obj obj) {
		assert obj != null;
		path.addLast(obj);
	}
	
	public Obj removeLast() {
		return path.removeLast();
	}

	@Override
	public Iterator<Obj> iterator() {
		return path.iterator();
	}

	public Iterator<Obj> descendingIterator() {
		return path.descendingIterator();
	}
}
