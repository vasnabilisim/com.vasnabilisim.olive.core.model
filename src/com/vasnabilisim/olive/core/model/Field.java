package com.vasnabilisim.olive.core.model;

import java.io.Serializable;

import com.vasnabilisim.util.EqualsBuilder;

/**
 * Holds meta information of report object fields.
 * 
 * @author Menderes Fatih GUVEN
 */
public class Field implements Serializable {
	private static final long serialVersionUID = -1345084361135819852L;

	/**
	 * Type of the field.
	 */
	private ObjType type = ObjType.Other;
	
	/**
	 * Name of the field.
	 */
	private String name = null;

	/**
	 * Returns a type representation string.
	 * @param type
	 * @return
	 */
	public static String getTypeAsString(ObjType type) {
		return type.name();
	}

	/**
	 * Type-Value constructor.
	 * @param type
	 * @param name
	 */
	public Field(ObjType type, String name) {
	    this.type = type;
	    this.name = name;
	}
	
	/**
	 * Returns the type.
	 * @return
	 */
	public ObjType getType() {
	    return type;
	}
	
	/**
	 * Returns the name.
	 * @return
	 */
	public String getName() {
	    return name;
	}
	
	/**
	 * Is this field a parent of any child.
	 * @return
	 */
	public boolean hasChild() {
		return type == ObjType.Other || type == ObjType.List;
	}

	/**
	 * Creates and returns a report field.
	 * @param type
	 * @param name
	 * @return
	 */
	public static Field createField(ObjType type, String name) {
	    return new Field(type, name);
	}
	
	public static Field createBoolean(String name) {
	    return createField(ObjType.Boolean, name);
	}
	
	public static Field createCollection(String name) {
	    return createField(ObjType.List, name);
	}
	
	public static Field createDateTime(String name) {
	    return createField(ObjType.DateTime, name);
	}

	public static Field createDate(String name) {
	    return createField(ObjType.Date, name);
	}

	public static Field createTime(String name) {
	    return createField(ObjType.Time, name);
	}

//	public static Field createImage(String name) {
//	    return createField(ObjType.Image, name);
//	}

	public static Field createByteArray(String name) {
	    return createField(ObjType.ByteArray, name);
	}

	public static Field createNull() {
	    return createField(ObjType.Other, "<null>");
	}

	public static Field createInteger(String name) {
	    return createField(ObjType.Integer, name);
	}

	public static Field createDecimal(String name) {
	    return createField(ObjType.Decimal, name);
	}

	public static Field createObject(String name) {
	    return createField(ObjType.Object, name);
	}

	public static Field createString(String name) {
	    return createField(ObjType.String, name);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof Field))
			return false;
		Field other = (Field)obj;
		return new EqualsBuilder().append(this.name, other.name).toBoolean();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}
}
