package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.util.LinkedList;

import com.vasnabilisim.util.EqualsBuilder;

/**
 * @author Menderes Fatih GUVEN
 */
public class ModelInfoFolder implements 
		Serializable, 
		com.vasnabilisim.util.Cloneable<ModelInfoFolder>, 
		java.lang.Cloneable, 
		Comparable<ModelInfoFolder> 
{
	private static final long serialVersionUID = -9040899179168649382L;

	/**
	 * Parent folder.
	 */
	private ModelInfoFolder parent = null;
	
	/**
	 * Name of the folder.
	 */
	private String name;
	
	/**
	 * Information about folder.
	 */
	private String information;

	/**
	 * Sub folders.
	 */
	private LinkedList<ModelInfoFolder> folderList = new LinkedList<ModelInfoFolder>();
	
	/**
	 * Sub models.
	 */
	private LinkedList<ModelInfo> modelList = new LinkedList<ModelInfo>();

	/**
	 * Default constructor.
	 */
	public ModelInfoFolder() {
	}

	/**
	 * Value constructor.
	 * @param name
	 * @param information
	 */
	public ModelInfoFolder(String name, String information) {
		this.name = name;
		this.information = information;
	}

	/**
	 * Copy constructor. 
	 * Copies the parent but not children.
	 * @param source
	 */
	public ModelInfoFolder(ModelInfoFolder source) {
		this.name = source.name;
		this.information = source.information;
		if(source.parent != null)
			this.parent = new ModelInfoFolder(source.parent);
	}
	
	@Override
	protected ModelInfoFolder clone() {
		return cloneObject();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public ModelInfoFolder cloneObject() {
		return new ModelInfoFolder(this);
	}

	/**
	 * Returns parent folder.
	 * @return
	 */
	public ModelInfoFolder getParent() {
		return parent;
	}

	/**
	 * Sets parent folder.
	 * @param parent
	 */
	public void setParent(ModelInfoFolder parent) {
		this.parent = parent;
	}

	/**
	 * Returns the url.
	 * @return
	 */
	public String getUrl() {
		if(parent == null) //ROOT
			return null;
		String parentUrl = parent.getUrl();
		if(parentUrl == null)
			return name;
		return parentUrl + "." + name;
	}

	/**
	 * Returns the name.
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the information.
	 * @return
	 */
	public String getInformation() {
		return information;
	}
	
	/**
	 * Sets the information.
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}
	
	/**
	 * Returns the list of sub folders.
	 * @return
	 */
	public LinkedList<ModelInfoFolder> getFolderList() {
		return folderList;
	}

	/**
	 * Adds a sub folder.
	 * @param subFolder
	 */
	public void addFolder(ModelInfoFolder subFolder) {
		if(subFolder == null)
			return;
		folderList.add(subFolder);
		subFolder.setParent(this);
	}

	/**
	 * Removes the given sub folder.
	 * @param subFolder
	 */
	public void removeFolder(ModelInfoFolder subFolder) {
		if(subFolder == null)
			return;
		folderList.remove(subFolder);
		subFolder.setParent(null);
	}

	/**
	 * Returns the list of sub model infos.
	 * @return
	 */
	public LinkedList<ModelInfo> getModelList() {
		return modelList;
	}
	
	/**
	 * Adds a sub report model info.
	 * @param modelInfo
	 */
	public void addModel(ModelInfo modelInfo) {
		if(modelInfo == null)
			return;
		modelList.add(modelInfo);
		modelInfo.setParent(this);
	}

	/**
	 * Removes the given model info.
	 * @param modelInfo
	 */
	public void removeModel(ModelInfo modelInfo) {
		if(modelInfo == null)
			return;
		modelList.remove(modelInfo);
		modelInfo.setParent(null);
	}
	
	/**
	 * Returns the full path of the folder.
	 * @return
	 */
	public String getFullName() {
		if(parent == null)
			return null; //This is the root folder.
		String parentFullName = parent.getFullName();
		if(parentFullName == null)
			return name;
		return parentFullName + "/" + name;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof ModelInfoFolder))
			return false;
		ModelInfoFolder other = (ModelInfoFolder) obj;
		return new EqualsBuilder().append(this.parent, other.parent).append(this.name, other.name).toBoolean();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ModelInfoFolder other) {
		if(other == null)
			return 1;
		if(this.name == null) {
			if(other.name == null)
				return 0;
			return -1;
		}
		if(other.name == null)
			return 1;
		return this.name.compareTo(other.name);
	}
}
