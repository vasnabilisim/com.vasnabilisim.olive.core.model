package com.vasnabilisim.olive.core.model;

import java.io.Serializable;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.util.EqualsBuilder;

/**
 * All report models have an info field to hold meta information. 
 * This type used to create report models dynamically on runtime.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class ModelInfo implements 
		Serializable, 
		com.vasnabilisim.util.Cloneable<ModelInfo>, 
		Cloneable, Comparable<ModelInfo> 
{
	private static final long serialVersionUID = 6904586232914276949L;

	/**
	 * Parent folder.
	 */
	protected ModelInfoFolder parent = null;
	
	/**
	 * Name of the report model.
	 */
	protected String name;
	
	/**
	 * Information.
	 */
	protected String information;

	/**
	 * Default constructor.
	 */
	public ModelInfo() {
	}

	/**
	 * Value constructor.
	 * @param name
	 */
	public ModelInfo(String name) {
		this.name = name;
	}

	/**
	 * Value constructor.
	 * @param name
	 * @param information
	 */
	public ModelInfo(String name, String information) {
		this.name = name;
		this.information = information;
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	protected ModelInfo(ModelInfo source) {
		this.name = source.name;
		this.information = source.information;
	}

	/**
	 * Returns parent folder.
	 * @return
	 */
	public ModelInfoFolder getParent() {
		return parent;
	}

	/**
	 * Sets parent folder.
	 * @param parent
	 */
	public void setParent(ModelInfoFolder parent) {
		this.parent = parent;
	}
	
	/**
	 * Returns the type of the report model info.
	 * @return
	 */
	public abstract String getType();
	
	/**
	 * Returns the url.
	 * @return
	 */
	public String getUrl() {
		String parentUrl = parent.getUrl();
		if(parentUrl == null)
			return name;
		return parentUrl + "." + name;
	}
	
	/**
	 * Returns the name.
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the information.
	 * @return
	 */
	public String getInformation() {
		return information;
	}
	
	/**
	 * Sets the information.
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}
	
	/**
	 * Returns the icon key of this type of report models.
	 * @return
	 */
	public abstract String getIconKey();
	
	/**
	 * Returns the full path of the report model info.
	 * @return
	 */
	public String getFullName() {
		if(parent == null)
			return name;
		String parentFullName = parent.getFullName();
		if(parentFullName == null)
			return name;
		return parentFullName + "/" + name;
	}
	
	/**
	 * Returns a new Report model of this type. 
	 * Also initializes the report model if given params is not null.
	 * 
	 * @param params
	 * @return
	 * @throws BaseException
	 */
	public abstract Model newInstance(Parameters params) throws BaseException;

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof ModelInfo))
			return false;
		ModelInfo other = (ModelInfo)obj;
		return new EqualsBuilder().append(this.parent, other.parent).append(this.name, other.name).toBoolean();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(ModelInfo other) {
		if(other == null)
			return 1;
		return this.name.compareTo(other.name);
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected ModelInfo clone() {
		return cloneObject();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public abstract ModelInfo cloneObject();
}
