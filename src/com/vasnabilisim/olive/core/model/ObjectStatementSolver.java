package com.vasnabilisim.olive.core.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.StringTokenizer;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.statement.VariableStatement;
import com.vasnabilisim.util.Iterative;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjectStatementSolver extends AbstractStatementSolver {

	private ObjectModel objectModel = null;

	/**
	 * Model constructor.
	 * @param objectModel
	 */
	public ObjectStatementSolver(ObjectModel objectModel) {
		this.objectModel = objectModel;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.StatementSolver#getModel()
	 */
	@Override
	public ObjectModel getModel() {
		return objectModel;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.AbstractStatementSolver#solveVariableStatement(com.vasnabilisim.olive.core.model.statement.VariableStatement)
	 */
	@Override
	public Object solveVariableStatement(VariableStatement statement) throws BaseException {
		if(statement == null || objectModel == null)
			return null;
		String fullVariableName = statement.getText();
		if(StringUtil.isEmpty(fullVariableName))
			return null;
		StringTokenizer tokenizer = new StringTokenizer(fullVariableName, ".", false);
		Object object = objectModel;
		Class<?> objectClass = null;
		String variableName = null;
		String buffer = null;
		while(tokenizer.hasMoreTokens()) {
			if(object == null) {
				String message = String.format("Unable to access %s; %s is null.", fullVariableName, buffer);
				throw new ErrorException(message);
			}
			
			if(object instanceof Iterative) {
				Iterative<?> iterative = (Iterative<?>)object;
				object = iterative.current();
			}
			
			objectClass = object.getClass();
			variableName = tokenizer.nextToken();
			if(buffer == null)
				buffer = variableName;
			else
				buffer += "." + variableName;

			String methodName = "get" + variableName;
			Method method;
			try {
				method = objectClass.getMethod(methodName);
			} catch (NoSuchMethodException | SecurityException e) {
				String message = String.format("Unable to access %s method of %s.", methodName, objectClass.getName());
				throw new ErrorException(message, e);
			}
			try {
				object = method.invoke(object);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				String message = String.format("Unable to invoke %s method of %s.", methodName, objectClass.getName());
				throw new ErrorException(message, e);
			}
		}
		return object;
	}

}
