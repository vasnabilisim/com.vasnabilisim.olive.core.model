package com.vasnabilisim.olive.core.model;

import java.util.Base64;

import com.vasnabilisim.olive.core.Formats;

/**
 * Type of the field.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum ObjType {
	Other(
		new Handler() {
			public String getInitial() { return "Oth"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.lang.Object.class; }
			public Class<?> getJdbcClass() { return null; }
			public java.lang.Object toObject(java.lang.Object jdbc) { 
				throw new UnsupportedOperationException(); 
			}
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromObjectToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultObject() { throw new UnsupportedOperationException(); }
			public java.lang.Object toJdbc(java.lang.Object object) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultJdbc() { throw new UnsupportedOperationException(); }
			public java.lang.String toXsdDataType() { throw new UnsupportedOperationException(); }
		}
	), 
	String(
		new Handler() {
			public String getInitial() { return "Str"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.lang.String.class; }
			public Class<?> getJdbcClass() { return java.lang.String.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return stringValue; }
			public java.lang.String fromObjectToString(java.lang.Object value) { return (String)value; }
			public java.lang.Object getDefaultObject() { return ""; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return stringValue; }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return (String)value; }
			public java.lang.Object getDefaultJdbc() { return ""; }
			public java.lang.String toXsdDataType() { return "xs:string"; }
		}
	), 
	Boolean(
		new Handler() {
			public String getInitial() { return "Bln"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.lang.Boolean.class; }
			public Class<?> getJdbcClass() { return java.lang.Boolean.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return java.lang.Boolean.valueOf(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultObject() { return java.lang.Boolean.FALSE; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return java.lang.Boolean.valueOf(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultJdbc() { return java.lang.Boolean.FALSE; }
			public java.lang.String toXsdDataType() { return "xs:boolean"; }
		}
	), 
	Integer(
		new Handler() {
			public String getInitial() { return "Int"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.lang.Long.class; }
			public Class<?> getJdbcClass() { return java.lang.Long.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return java.lang.Long.valueOf(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultObject() { return 0L; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return java.lang.Long.valueOf(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultJdbc() { return 0L; }
			public java.lang.String toXsdDataType() { return "xs:integer"; }
		}
	), 
	Decimal(
		new Handler() {
			public String getInitial() { return "Dec"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.math.BigDecimal.class; }
			public Class<?> getJdbcClass() { return java.math.BigDecimal.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return new java.math.BigDecimal(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultObject() { return java.math.BigDecimal.ZERO; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return new java.math.BigDecimal(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return value.toString(); }
			public java.lang.Object getDefaultJdbc() { return java.math.BigDecimal.ZERO; }
			public java.lang.String toXsdDataType() { return "xs:decimal"; }
		}
	), 
	DateTime(
		new Handler() {
			public String getInitial() { return "Dtt"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.time.LocalDateTime.class; }
			public Class<?> getJdbcClass() { return java.sql.Timestamp.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return ((java.sql.Timestamp)jdbc).toLocalDateTime(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return Formats.parseLocalDateTime(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return Formats.formatLocalDateTime((java.time.LocalDateTime)value); }
			public java.lang.Object getDefaultObject() { return java.time.LocalDateTime.now(); }
			public java.lang.Object toJdbc(java.lang.Object object) { return java.sql.Timestamp.valueOf((java.time.LocalDateTime)object); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return Formats.parseSqlTimestamp(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return Formats.formatSqlTimestamp((java.sql.Timestamp)value); }
			public java.lang.Object getDefaultJdbc() { return new java.sql.Timestamp(System.currentTimeMillis()); }
			public java.lang.String toXsdDataType() { return "xs:dateTime"; }
		}
	), 
	Date(
		new Handler() {
			public String getInitial() { return "Dte"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.time.LocalDate.class; }
			public Class<?> getJdbcClass() { return java.sql.Date.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return ((java.sql.Date)jdbc).toLocalDate(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return Formats.parseLocalDate(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return Formats.formatLocalDate((java.time.LocalDate)value); }
			public java.lang.Object getDefaultObject() { return java.time.LocalDate.now(); }
			public java.lang.Object toJdbc(java.lang.Object object) { return java.sql.Date.valueOf((java.time.LocalDate)object); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return Formats.parseSqlDate(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return Formats.formatSqlDate((java.sql.Date)value); }
			public java.lang.Object getDefaultJdbc() { return new java.sql.Date(System.currentTimeMillis()); }
			public java.lang.String toXsdDataType() { return "xs:date"; }
		}
	), 
	Time(
		new Handler() {
			public String getInitial() { return "Tme"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return java.time.LocalTime.class; }
			public Class<?> getJdbcClass() { return java.sql.Time.class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return ((java.sql.Time)jdbc).toLocalTime(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return Formats.parseLocalTime(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return Formats.formatLocalTime((java.time.LocalTime)value); }
			public java.lang.Object getDefaultObject() { return java.time.LocalTime.now(); }
			public java.lang.Object toJdbc(java.lang.Object object) { return java.sql.Time.valueOf((java.time.LocalTime)object); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return Formats.parseSqlTime(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return Formats.formatSqlTime((java.sql.Time)value); }
			public java.lang.Object getDefaultJdbc() { return new java.sql.Time(System.currentTimeMillis()); }
			public java.lang.String toXsdDataType() { return "xs:time"; }
		}
	), 
	/*
	Image(
		new Handler() {
			public String getInitial() { return "Img"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return byte[].class; }
			public Class<?> getJdbcClass() { return byte[].class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return stringValue == null ? null : Base64.getUrlDecoder().decode(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return value == null ? null : Base64.getUrlEncoder().encodeToString((byte[])value); }
			public java.lang.Object getDefaultObject() { return null; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return stringValue == null ? null : Base64.getUrlDecoder().decode(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return value == null ? null : Base64.getUrlEncoder().encodeToString((byte[])value); }
			public java.lang.Object getDefaultJdbc() { return null; }
			public java.lang.String toXsdDataType() { return "xs:base64Binary"; }
		}
	),
	*/
	ByteArray(
		new Handler() {
			public String getInitial() { return "Byt"; }
			public boolean isSimple() { return true; }
			public Class<?> getObjectClass() { return byte[].class; }
			public Class<?> getJdbcClass() { return byte[].class; }
			public java.lang.Object toObject(java.lang.Object jdbc) { return jdbc; }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { return stringValue == null ? null : Base64.getUrlDecoder().decode(stringValue); }
			public java.lang.String fromObjectToString(java.lang.Object value) { return value == null ? null : Base64.getUrlEncoder().encodeToString((byte[])value); }
			public java.lang.Object getDefaultObject() { return null; }
			public java.lang.Object toJdbc(java.lang.Object object) { return object; }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { return stringValue == null ? null : Base64.getUrlDecoder().decode(stringValue); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { return value == null ? null : Base64.getUrlEncoder().encodeToString((byte[])value); }
			public java.lang.Object getDefaultJdbc() { return null; }
			public java.lang.String toXsdDataType() { return "xs:base64Binary"; }
		}
	), 
	Object(
		new Handler() {
			public String getInitial() { return "Obj"; }
			public boolean isSimple() { return false; }
			public Class<?> getObjectClass() { return Obj.class; }
			public Class<?> getJdbcClass() { throw new UnsupportedOperationException(); }
			public java.lang.Object toObject(java.lang.Object jdbc) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromObjectToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultObject() { throw new UnsupportedOperationException(); }
			public java.lang.Object toJdbc(java.lang.Object object) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultJdbc() { throw new UnsupportedOperationException(); }
			public java.lang.String toXsdDataType() { throw new UnsupportedOperationException(); }
		}
	), 
	List(
		new Handler() {
			public String getInitial() { return "Lst"; }
			public boolean isSimple() { return false; }
			public Class<?> getObjectClass() { return Obj.class; }
			public Class<?> getJdbcClass() { throw new UnsupportedOperationException(); }
			public java.lang.Object toObject(java.lang.Object jdbc) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromObjectToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultObject() { throw new UnsupportedOperationException(); }
			public java.lang.Object toJdbc(java.lang.Object object) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultJdbc() { throw new UnsupportedOperationException(); }
			public java.lang.String toXsdDataType() { throw new UnsupportedOperationException(); }
		}
	), 
	Any(
		new Handler() {
			public String getInitial() { return "Any"; }
			public boolean isSimple() { return false; }
			public Class<?> getObjectClass() { return java.lang.Object.class; }
			public Class<?> getJdbcClass() { throw new UnsupportedOperationException(); }
			public java.lang.Object toObject(java.lang.Object jdbc) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToObject(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromObjectToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultObject() { throw new UnsupportedOperationException(); }
			public java.lang.Object toJdbc(java.lang.Object object) { throw new UnsupportedOperationException(); }
			public java.lang.Object fromStringToJdbc(java.lang.String stringValue) { throw new UnsupportedOperationException(); }
			public java.lang.String fromJdbcToString(java.lang.Object value) { throw new UnsupportedOperationException(); }
			public java.lang.Object getDefaultJdbc() { throw new UnsupportedOperationException(); }
			public java.lang.String toXsdDataType() { throw new UnsupportedOperationException(); }
		}
	); 
	
	Handler handler;

	interface Handler {
		String getInitial();
		boolean isSimple();
		Class<?> getObjectClass();
		Class<?> getJdbcClass();
		Object toObject(Object jdbc);
		Object fromStringToObject(String stringValue);
		String fromObjectToString(Object value);
		Object getDefaultObject();
		Object toJdbc(Object object);
		Object fromStringToJdbc(String stringValue);
		String fromJdbcToString(Object value);
		Object getDefaultJdbc();
		String toXsdDataType();
	}
	
	
	ObjType(Handler handler) {
		this.handler = handler;
	}
	
	public String getInitial() {
		return handler.getInitial();
	}
	
	public boolean isSimple() {
		return handler.isSimple();
	}
	
	public Class<?> getObjectClass() {
		return handler.getObjectClass();
	}
	
	public Class<?> getJdbcClass() {
		return handler.getJdbcClass();
	}
	
	public Object toObject(Object jdbc) {
		if(jdbc == null)
			return null;
		return handler.toObject(jdbc);
	}
	
	public Object fromStringToObject(String stringValue) {
		return stringValue == null ? null : handler.fromStringToObject(stringValue);
	}
	
	public String fromObjectToString(Object value) {
		return value == null ? null : handler.fromObjectToString(value);
	}
	
	public Object getDefaultObject() {
		return handler.getDefaultObject();
	}
	
	public Object toJdbc(Object object) {
		return object == null ? null : handler.toJdbc(object);
	}

	public Object fromStringToJdbc(String stringValue) {
		return stringValue == null ? null : handler.fromStringToJdbc(stringValue);
	}
	
	public String fromJdbcToString(Object value) {
		return value == null ? null : handler.fromJdbcToString(value);
	}
	
	public Object getDefaultJdbc() {
		return handler.getDefaultJdbc();
	}
	
	public String toXsdDataType() {
		return handler.toXsdDataType();
	}
	
	/**
	 * Returns the corresponding type of given sql type.
	 * @param type
	 * @return
	 */
	public static ObjType fromSqlType(int type) {
		switch(type) {
			case java.sql.Types.TINYINT		: 
			case java.sql.Types.SMALLINT	: 
			case java.sql.Types.INTEGER		: 
			case java.sql.Types.BIGINT		: return Integer;
			case java.sql.Types.FLOAT		: 
			case java.sql.Types.REAL		: 
			case java.sql.Types.DOUBLE		: 
			case java.sql.Types.NUMERIC		: 
			case java.sql.Types.DECIMAL		: return Decimal;
			case java.sql.Types.CHAR		: 
			case java.sql.Types.VARCHAR		: 
			case java.sql.Types.LONGVARCHAR	: 
			case java.sql.Types.NCHAR		: 
			case java.sql.Types.NVARCHAR	: 
			case java.sql.Types.CLOB		: return String;
			case java.sql.Types.DATE		: return Date;
			case java.sql.Types.TIME		: return Time;
			case java.sql.Types.TIMESTAMP	: return DateTime;
			case java.sql.Types.BIT			: 
			case java.sql.Types.BOOLEAN		: return Boolean;
			default							: return Other;
		}
	}
}
