package com.vasnabilisim.olive.core.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjectModelInfo extends ModelInfo {
	private static final long serialVersionUID = 6469227480932853679L;

	/**
	 * Class name of the model.
	 */
	private String className = null;

	/**
	 * Default constructor.
	 */
	public ObjectModelInfo() {
		super();
	}

	/**
	 * Value constructor.
	 * 
	 * @param name
	 * @param information
	 * @param className
	 */
	public ObjectModelInfo(String name, String information, String className) {
		super(name, information);
		this.className = className;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public ObjectModelInfo(ObjectModelInfo source) {
		super(source);
		this.className = source.className;
	}

	/**
	 * Returns class name.
	 * @return
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Sets class name.
	 * @param className
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#getType()
	 */
	@Override
	public String getType() {
		return "object";
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#getIconKey()
	 */
	@Override
	public String getIconKey() {
		return "ObjectModel.png";
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#newInstance(com.vasnabilisim.olive.core.model.Parameters)
	 */
	@Override
	public Model newInstance(Parameters params) throws BaseException {
		Class<? extends ObjectModel> reportModelClass = null;
		try {
			reportModelClass = com.vasnabilisim.util.ClassLoader.loadClass(className, ObjectModel.class);
		} catch(ClassCastException e) {
			//ClassNotFoundException
			String message = String.format("Class %s is not a model type.", className);
			throw new ErrorException(message, e);
		} catch (ClassNotFoundException e) {
			String message = String.format("Unable to find model class %s.", className);
			throw new ErrorException(message, e);
		}

		Constructor<? extends ObjectModel> reportModelConstructor = null;
		try {
			reportModelConstructor = reportModelClass.getConstructor(ObjectModelInfo.class);
		} catch (NoSuchMethodException | SecurityException e) {
			String message = String.format("Unable to find info constructor in model %s.", className);
			throw new ErrorException(message);
		}
		ObjectModel model = null;
		try {
			model = reportModelConstructor.newInstance(this);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			String message = String.format("Unable to create model %s.", className);
			throw new ErrorException(message, e);
		}
		
		if(params != null)
			model.initialize(params);
		return model;
	}


	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof ObjectModelInfo))
			return false;
		ObjectModelInfo other = (ObjectModelInfo)obj;
		return this.className.equals(other.className);
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#cloneObject()
	 */
	@Override
	public ObjectModelInfo cloneObject() {
		return new ObjectModelInfo(this);
	}

}
