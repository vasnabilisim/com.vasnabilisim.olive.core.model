package com.vasnabilisim.olive.core.model.sql;

import java.util.StringTokenizer;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.AbstractStatementSolver;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.statement.VariableStatement;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlStatementSolver extends AbstractStatementSolver {

	SqlModel model;

	/**
	 * Model constructor.
	 * 
	 * @param model
	 */
	public SqlStatementSolver(SqlModel model) {
		this.model = model;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.StatementSolver#getModel()
	 */
	@Override
	public SqlModel getModel() {
		return model;
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.AbstractStatementSolver#solveVariableStatement(com.vasnabilisim.olive.core.model.statement.VariableStatement)
	 */
	@Override
	public Object solveVariableStatement(VariableStatement statement) throws BaseException {
		if(statement == null || model == null)
			return null;
		String fullVariableName = statement.getText();
		if(StringUtil.isEmpty(fullVariableName))
			return null;
		StringTokenizer tokenizer = new StringTokenizer(fullVariableName, ".", false);
		Obj root = model.getRootObj();
		String token = null;
		StringBuilder buffer = new StringBuilder(fullVariableName.length());
		while(tokenizer.hasMoreTokens()) {
			if(root == null) {
				String message = String.format("Unable to access %s; %s is null.", fullVariableName, buffer);
				throw new ErrorException(message);
			}
			
			token = tokenizer.nextToken();
			if(buffer.length() == 0)
				buffer.append(token);
			else
				buffer.append('.').append(token);
			
			ObjInfo childResultInfo = root.getInfo().getChild(token);
			if(childResultInfo == null) {
				String message = String.format("Unable to solve %s. Statement %s failed.", fullVariableName, buffer);
				throw new ErrorException(message);
			}
			Object value = root.getValue(childResultInfo.getIndex());
			if(!tokenizer.hasMoreTokens()) 
				return value;
			if(!(value instanceof Obj)) {
				String message = String.format("Unable to solve %s. Statement %s failed.", fullVariableName, buffer);
				throw new ErrorException(message);
			}
			if(childResultInfo.getType() == ObjType.Object)
				root = (Obj) value;
			else if(childResultInfo.getType() == ObjType.List)
				root = (Obj) ((Obj)value).current();
		}
		return null;
	}

}
