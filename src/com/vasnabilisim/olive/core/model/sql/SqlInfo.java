package com.vasnabilisim.olive.core.model.sql;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.util.CloneableArrayList;
import com.vasnabilisim.util.Pair;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlInfo implements
		Comparable<SqlInfo>, 
		com.vasnabilisim.util.Cloneable<SqlInfo>,
		java.lang.Cloneable, 
		Serializable 
{
	private static final long serialVersionUID = -7430191619614786227L;

	/**
	 * Parent query.
	 */
	SqlInfo parent = null;
	
	/**
	 * Type name of the query.
	 */
	String typeName = null;
	
	/**
	 * Field name of the query. 
	 */
	String fieldName = null;
	
	/**
	 * Type of the query.
	 */
	SqlType type = SqlType.QueryMultiple;
	
	/**
	 * Variables mandatory flag.
	 */
	Boolean variablesMandatory = Boolean.FALSE;
	
	/**
	 * sql string.
	 */
	String query = null;

	/**
	 * column infos.
	 */
	CloneableArrayList<ObjInfo> columnList = null;
	
	/**
	 * Query part list.
	 */
	QueryPartList queryPartList = null;
	
	List<String> parameterNameList = null;
	
	List<String> variableNameList = null;
	
	/**
	 * Sub queries.
	 */
	SqlInfoList sqlInfoList = null;
	
	/**
	 * Default constructor.
	 */
	public SqlInfo() {
		columnList = new CloneableArrayList<>();
		queryPartList = new QueryPartList();
		parameterNameList = new ArrayList<>(5);
		variableNameList = new ArrayList<>(2);
		sqlInfoList = new SqlInfoList();
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public SqlInfo(SqlInfo source) {
		this.parent = null;
		this.typeName = source.typeName;
		this.fieldName = source.fieldName;
		this.type = source.type;
		this.variablesMandatory = source.variablesMandatory;
		this.query = source.query;
		this.sqlInfoList = source.sqlInfoList.cloneObject();
		this.queryPartList = source.queryPartList.cloneObject();
		this.parameterNameList = new ArrayList<>(source.parameterNameList);
		this.variableNameList = new ArrayList<>(source.variableNameList);
	}
	
	@Override
	public int compareTo(SqlInfo other) {
		if(other == null)
			return 1;
		if(this == other)
			return 0;
		return Integer.compare(this.hashCode(), other.hashCode());
		/*
		int result = this.typeName.compareTo(other.typeName);
		if(result != 0)
			return result;
		result = this.fieldName.compareTo(other.fieldName);
		return result == 0 ? this.query.compareTo(other.query) : result;
		*/
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public SqlInfo cloneObject() {
		return new SqlInfo(this);
	}

	/**
	 * Returns the type name of the query.
	 * @return
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * Sets the type name of the query.
	 * @param typeName
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	/**
	 * Returns the field name of the query.
	 * @return
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name of the query.
	 * @param fieldName
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Returns the type of the query.
	 * @return
	 */
	public SqlType getType() {
		return type;
	}

	/**
	 * Sets the type of the query.
	 * @param type
	 */
	public void setType(SqlType type) {
		this.type = type;
	}
	
	/**
	 * Returns the variables mandatory flag.
	 * @return
	 */
	public Boolean getVariablesMandatory() {
		return variablesMandatory;
	}
	
	/**
	 * Sets the variables mandatory flag.
	 * @param variablesMandatory
	 */
	public void setVariablesMandatory(Boolean variablesMandatory) {
		this.variablesMandatory = variablesMandatory == null ? Boolean.FALSE : variablesMandatory;
	}

	/**
	 * Returns the query string.
	 * @return
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Sets the query string.
	 * @param query
	 */
	public void setQuery(String query) {
		this.query = query;
		parseQuery();
	}

	/**
	 * Returns column info list.
	 * @return
	 */
	public CloneableArrayList<ObjInfo> getColumnList() {
		return columnList;
	}
	
	/**
	 * Returns given named column info. 
	 * Returns null if no such column info exists.
	 * @param name
	 * @return
	 */
	public ObjInfo getColumn(String name) {
		for(ObjInfo column : columnList)
			if(column.getFieldName().equals(name))
				return column;
		return null;
	}
	
	/**
	 * Adds column info
	 * @param column
	 */
	public void addColumn(ObjInfo column) {
		columnList.add(column);
	}
	
	/**
	 * Returns the parent query.
	 * @return
	 */
	public SqlInfo getParent() {
		return parent;
	}

	/**
	 * Sets the parent query.
	 * @param sqlInfo
	 */
	public void setParent(SqlInfo sqlInfo) {
		this.parent = sqlInfo;
	}
	
	/**
	 * Returns the list of sub queries.
	 * @return
	 */
	public List<SqlInfo> getSqlInfoList() {
		return sqlInfoList;
	}
	
	/**
	 * Returns the number of sub sql infos.
	 * @return
	 */
	public int getChildCount() {
		return sqlInfoList.size();
	}
	
	/**
	 * Adds given sub query.
	 * @param sqlInfo
	 */
	public void addSqlInfo(SqlInfo sqlInfo) {
		if(sqlInfo == null)
			return;
		sqlInfoList.add(sqlInfo);
		sqlInfo.setParent(this);
	}
	
	/**
	 * Removes given sub query.
	 * @param sqlInfo
	 */
	public void removeSqlInfo(SqlInfo sqlInfo) {
		if(sqlInfo == null)
			return;
		if(sqlInfoList.remove(sqlInfo))
			sqlInfo.setParent(this);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(query == null)
			return "";
		return typeName + " {" + query + "}";
	}
	
	/**
	 * Returns the list of query parts.
	 * @return
	 */
	public QueryPartList getQueryPartList() {
		return queryPartList;
	}
	
	public List<String> getParameterNameList() {
		return parameterNameList;
	}
	
	public List<String> getVariableNameList() {
		return variableNameList;
	}
	
	/**
	 * Parses the query string. 
	 * Fills query part list.
	 */
	private void parseQuery() {
		//TODO use regex
		queryPartList.clear();
		if(query == null)
			return;
		StringTokenizer tokenizer = new StringTokenizer(query, "$)", true);
		String token1 = null;
		String token2 = null;
		String token3 = null;
		StringBuilder builder = new StringBuilder(query.length());
		while(tokenizer.hasMoreTokens()) {
			token1 = tokenizer.nextToken();
			if("$".equals(token1)) {
				if(tokenizer.hasMoreTokens()) {
					token2 = tokenizer.nextToken();
					if(tokenizer.hasMoreTokens()) {
						token3 = tokenizer.nextToken();
						if( (token2.startsWith("P(") || token2.startsWith("p(")) && token3.equals(")")) {
							if(builder.length() > 0) {
								queryPartList.add(new QueryPart(QueryPartType.String, builder.toString()));
								builder.setLength(0);
							}
							String parameter = token2.substring(2, token2.length()).trim();
							queryPartList.add(new QueryPart(QueryPartType.Parameter, parameter));
							parameterNameList.add(parameter);
						}
						else if( (token2.startsWith("V(") || token2.startsWith("v(")) && token3.equals(")")) {
							if(builder.length() > 0) {
								queryPartList.add(new QueryPart(QueryPartType.String, builder.toString()));
								builder.setLength(0);
							}
							String variable = token2.substring(2, token2.length()).trim();
							queryPartList.add(new QueryPart(QueryPartType.Variable, variable));
							variableNameList.add(variable);
						}
						else
							builder.append(token1).append(token2).append(token3);
					}//if(tokenizer.hasMoreTokens())
					else
						builder.append(token1).append(token2);
				}//if(tokenizer.hasMoreTokens())
				else {
					builder.append(token1);
				}
			}//if("$".equals(token1))
			else {
				builder.append(token1);
			}
		}//while(tokenizer.hasMoreTokens())
		if(builder.length() > 0) 
			queryPartList.add(new QueryPart(QueryPartType.String, builder.toString()));
	}
	
	public Pair<String, List<Pair<QueryPartType, String>>> createStatement() throws BaseException {
		StringBuilder sqlBuilder = new StringBuilder(query.length());
		ArrayList<Pair<QueryPartType, String>> argumentList = new ArrayList<>();
		for(SqlInfo.QueryPart queryPart : queryPartList) {
			if(queryPart.getType() == QueryPartType.String) {
				sqlBuilder.append(queryPart.getValue());
			}
			else {
				sqlBuilder.append('?');
				argumentList.add(new Pair<>(queryPart.getType(), queryPart.getValue()));
			}
		}
		return new Pair<>(sqlBuilder.toString(), argumentList);
	}

	public static class Content {
		public SqlInfo sqlInfo;
		
		public String query;
		public List<Pair<QueryPartType, String>> argumentList;

		public PreparedStatement statement;
		public ResultSet resultSet;
		public int colCount;
		
		public ObjInfo objInfo;
	}
	
	public static enum QueryPartType {
		String, 
		Parameter,
		Variable;
	}
	
	public static class QueryPart implements Serializable,
			com.vasnabilisim.util.Cloneable<QueryPart>,
			java.lang.Cloneable 
	{
		private static final long serialVersionUID = -3287134061745518553L;

		QueryPartType type;
		String value = null;

		QueryPart(QueryPartType type, String value) {
			this.type = type;
			this.value = value;
		}

		QueryPart(QueryPart source) {
			this.type = source.type;
			this.value = source.value;
		}
		
		public QueryPartType getType() {
			return type;
		}

		public String getValue() {
			return value;
		}

		@Override
		protected Object clone() {
			return cloneObject();
		}
		
		@Override
		public QueryPart cloneObject() {
			return new QueryPart(this);
		}
	}

	public static class QueryPartList extends ArrayList<QueryPart> implements
			com.vasnabilisim.util.Cloneable<QueryPartList>,
			java.lang.Cloneable, Serializable 
	{
		private static final long serialVersionUID = -5981373952617581011L;

		public QueryPartList() {
			super();
		}

		public QueryPartList(QueryPartList source) {
			super();
			for (QueryPart queryPart : source)
				this.add(queryPart.cloneObject());
		}

		@Override
		public QueryPartList cloneObject() {
			return new QueryPartList(this);
		}
	}
}
