package com.vasnabilisim.olive.core.model.sql;

import java.io.Serializable;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.jdbc.DataSourceInfo;
import com.vasnabilisim.olive.core.model.ModelInfo;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.Parameters;

/**
 * Sql Model Info.
 * @author Menderes Fatih GUVEN
 */
public class SqlModelInfo extends ModelInfo implements Serializable {
	private static final long serialVersionUID = -7396341317138878278L;

	/**
	 * Data source of the model.
	 */
	private DataSourceInfo dataSourceInfo = null;
	
	/**
	 * Parameter list of model.
	 */
	private ObjInfoList parameterInfoList = null;
	
	/**
	 * Root of queries.
	 */
	private SqlInfo sqlInfo = null;
	
	/**
	 * Default constructor.
	 */
	public SqlModelInfo() {
		super();
		parameterInfoList = new ObjInfoList();
		sqlInfo = new SqlInfo();
		sqlInfo.setTypeName("Root");
		sqlInfo.setFieldName("Root");
	}

	/**
	 * Name constructor
	 * @param name
	 */
	public SqlModelInfo(String name) {
		super(name);
		parameterInfoList = new ObjInfoList();
		sqlInfo = new SqlInfo();
		sqlInfo.setTypeName("Root");
		sqlInfo.setFieldName("Root");
	}

	/**
	 * Value constructor
	 * @param name
	 * @param information
	 */
	public SqlModelInfo(String name, String information) {
		super(name, information);
		parameterInfoList = new ObjInfoList();
		sqlInfo = new SqlInfo();
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public SqlModelInfo(SqlModelInfo source) {
		super(source);
		this.dataSourceInfo = source.dataSourceInfo.cloneObject();
		this.parameterInfoList = source.parameterInfoList.cloneObject();
		this.sqlInfo = source.sqlInfo.cloneObject();
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#cloneObject()
	 */
	@Override
	public SqlModelInfo cloneObject() {
		return new SqlModelInfo(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#getType()
	 */
	@Override
	public String getType() {
		return "sql";
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#getIconKey()
	 */
	@Override
	public String getIconKey() {
		return "SqlModel.png";
	}
	
	/**
	 * Returns the parameter info list.
	 * @return
	 */
	public ObjInfoList getParameterInfoList() {
		return parameterInfoList;
	}
	
	/**
	 * Returns the given named parameter info. 
	 * Returns null if no match found.
	 * @param name
	 * @return
	 */
	public ObjInfo getParameterInfo(String name) {
		if(name == null)
			return null;
		for(ObjInfo parameterInfo : parameterInfoList)
			if(name.equals(parameterInfo.getFieldName()))
				return parameterInfo;
		return null;
	}
	
	/**
	 * Adds given parameter info to parameter info list.
	 * @param parameterInfo
	 */
	public void addParameterInfo(ObjInfo parameterInfo) {
		parameterInfoList.addObjInfo(parameterInfo);
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.model.ModelInfo#newInstance(com.vasnabilisim.olive.core.model.Parameters)
	 */
	@Override
	public SqlModel newInstance(Parameters params) throws BaseException {
		SqlModel model = new SqlModel(this);
		if(params != null)
			model.initialize(params);
		return model;
	}
	
	/**
	 * Handles sql queries and return a root result object.
	 * @return
	 * @throws BaseException
	 */
	public Obj handle(Parameters params) throws BaseException {
		if(sqlInfo == null || sqlInfo.getSqlInfoList().isEmpty())
			return null;
		return handle(this.parameterInfoList.cloneAndFill(params, true));
	}

	/**
	 * Handles sql queries and return a root result object.
	 * @return
	 * @throws BaseException
	 */
	public Obj handle(ObjInfoList parameterInfoList) throws BaseException {
		if(sqlInfo == null || sqlInfo.getSqlInfoList().isEmpty())
			return null;
		SqlInfoHandler handler = new SqlInfoHandler(dataSourceInfo);
		Obj resultObject = handler.handle(sqlInfo, parameterInfoList);
		return resultObject;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj == this)
			return true;
		if(!(obj instanceof SqlModelInfo))
			return false;
		SqlModelInfo other = (SqlModelInfo)obj;
		return this.name.equals(other.name);
	}

	/**
	 * Returns the data source info.
	 * @return
	 */
	public DataSourceInfo getDataSourceInfo() {
		return dataSourceInfo;
	}

	/**
	 * Sets the data source info.
	 * @param dataSourceInfo
	 */
	public void setDataSourceInfo(DataSourceInfo dataSourceInfo) {
		this.dataSourceInfo = dataSourceInfo;
	}

	/**
	 * Returns the root of  queries.
	 * @return
	 */
	public SqlInfo getSqlInfo() {
		return sqlInfo;
	}

	/**
	 * Sets the root of queries.
	 * @param sqlInfo
	 */
	public void setSqlInfo(SqlInfo sqlInfo) {
		this.sqlInfo = sqlInfo;
	}
}
