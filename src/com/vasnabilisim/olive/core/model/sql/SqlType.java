package com.vasnabilisim.olive.core.model.sql;

/**
 * @author Menderes Fatih GUVEN
 */
public enum SqlType {
	Idle, QuerySingle, QueryMultiple, QueryValue, Update;
}
