
package com.vasnabilisim.olive.core.model.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.WarningException;
import com.vasnabilisim.jdbc.DataSourceInfo;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.core.model.ObjValue;
import com.vasnabilisim.olive.core.model.sql.SqlInfo.QueryPartType;
import com.vasnabilisim.util.Pair;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlInfoHandler {

	DataSourceInfo dataSourceInfo;
	
	Connection connection;
	TreeMap<SqlInfo, SqlInfoContent> sqlInfoContentMap = new TreeMap<>();
	
	public SqlInfoHandler(DataSourceInfo dataSourceInfo) {
		this.dataSourceInfo = dataSourceInfo;
	}

	public void createConnection() throws BaseException {
		if(connection != null)
			rollbackConnection();
		try {
			connection = dataSourceInfo.createConnection();
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			rollbackConnection();
			String message = String.format("Unable to connect datasource %s. Possible reason: %s.", 
					dataSourceInfo.getDataSourceUrl(), 
					e.getMessage());
			throw new WarningException(message, e);
		}
	}
	
	public void rollbackConnection() {
		if(connection != null)
			try {
				connection.rollback();
				connection.close();
			} catch(SQLException e) {
			} finally {
				connection = null;
				sqlInfoContentMap.clear();
			}
	}

	public void commitConnection() {
		if(connection != null)
			try {
				connection.commit();
				connection.close();
			} catch(SQLException e) {
			} finally {
				connection = null;
				sqlInfoContentMap.clear();
			}
	}
	
	public Obj handle(SqlInfo sqlInfo, ObjInfoList parameterInfoList) throws BaseException {
		if(connection == null)
			createConnection();
		
		ObjInfo rootResultInfo = new ObjInfo(sqlInfo.getChildCount(), sqlInfo.getFieldName(), 0, ObjType.Object);
		rootResultInfo.setTypeName(sqlInfo.getTypeName());
		Obj rootResultObject = new Obj(rootResultInfo, sqlInfo.getChildCount());

		for(SqlInfo childSqlInfo : sqlInfo.getSqlInfoList()) {
			try {
				handleInternal(childSqlInfo, parameterInfoList, rootResultObject);
			} catch (BaseException e) {
				rollbackConnection();
				throw e;
			}
		}
		
		//commitConnection();
		
		return rootResultObject;
	}
	
	public Pair<ObjInfo, Object> handle(SqlInfo sqlInfo, ObjInfoList parameterInfoList, Obj parentObject) throws BaseException {
		if(connection == null)
			createConnection();
		if(sqlInfo.getType() == SqlType.Idle)
			return new Pair<>(null, null);
		
		try {
			return handleInternal(sqlInfo, parameterInfoList, parentObject);
		} catch (BaseException e) {
			rollbackConnection();
			throw e;
		}
	}
	
	Pair<ObjInfo, Object> handleInternal(SqlInfo sqlInfo, ObjInfoList parameterInfoList, Obj parentObject) throws BaseException {
		SqlInfoContent content = getSqlInfoContent(sqlInfo);
		
		int index = 0;
		for(Pair<QueryPartType, String> attribute : content.argumentList) {
			++index;
			Object argumentJdbcValue = null;
			if(attribute.getFirst() == QueryPartType.Parameter) {
				ObjValue parameter = parameterInfoList.solve(attribute.getSecond());
				if(parameter.getValue() != null)
					argumentJdbcValue = parameter.getType().toJdbc(parameter.getValue());
				if(parameter.getValue() != null && argumentJdbcValue == null) {
					String message = String.format(
							"Unable to execute query {%s}. Unable to convert [%s] from (%s) to (%s).", 
							sqlInfo.getQuery(), 
							attribute.getSecond(), 
							parameter.getType().getObjectClass().getName(), 
							parameter.getType().getJdbcClass().getName()
					);
					throw new WarningException(message);
				}
			}
			else if(attribute.getFirst() == QueryPartType.Variable) {
				Object variableValue = null;
				ObjType variableType = null;
				Obj parentResultIte = parentObject;
				while(parentResultIte != null) {
					ObjInfo childResultInfo = parentResultIte.getInfo().getChild(attribute.getSecond());
					if(childResultInfo == null) {
						parentResultIte = parentResultIte.getParent();
						continue;
					}
					variableType = childResultInfo.getType();
					variableValue = parentResultIte.getValue(childResultInfo.getIndex());
					break;
				}
				
				if(variableType == null) {
					String message = String.format(
							"Unable to execute query {%s}. Unable to find variable [%s].", 
							content.sqlInfo.getQuery(), 
							attribute.getSecond()
					);
					throw new WarningException(message);
				}
				if(variableValue == null && sqlInfo.variablesMandatory) {
					String message = String.format(
							"Unable to execute query {%s}. Null variable [%s].", 
							content.sqlInfo.getQuery(), 
							attribute.getSecond()
					);
					throw new WarningException(message);
				}
				argumentJdbcValue = variableValue == null ? variableType.getDefaultJdbc() : variableType.toJdbc(variableValue);
			}
			
			try {
				content.statement.setObject(index, argumentJdbcValue);
			} catch (SQLException e) {
				String message = String.format(
						"Unable to execute query {%s}. Unable to set argument [%s].", 
						sqlInfo.getQuery(), 
						attribute.getSecond()
				);
				throw new WarningException(message, e);
			}
		}
		
		Object result = null;
		ResultSet resultSet = null;
		try {
			if(content.sqlInfo.getType() == SqlType.Update) {
				content.statement.executeUpdate();
				result = null;
			}
			else {
				resultSet = content.statement.executeQuery();
				ensureResultInfo(parentObject.getInfo(), content, resultSet);
				
				if(content.sqlInfo.getType() == SqlType.QueryValue) {
					Object value = null;
					if(resultSet.next())
						value = getObjectResult(resultSet, 1, content.objInfo.getType());
						//value = content.objInfo.getType().toObject(resultSet.getObject(1));
					parentObject.addValue(value);
					result = value;
				}
				else if(content.sqlInfo.getType() == SqlType.QuerySingle) {
					if(resultSet.next()) {
						Obj resultObj = new Obj(content.objInfo, content.colCount + content.sqlInfo.getChildCount());
						for(int col=1; col<=content.colCount; ++col) {
							ObjInfo childInfo = content.objInfo.getChild(col - 1);
							//resultObject.addValue(childResultInfo.getType().toObject(resultSet.getObject(col)));
							resultObj.addValue(getObjectResult(resultSet, col, childInfo.getType()));
						}
						for(SqlInfo childSqlInfo : sqlInfo.getSqlInfoList())
							handleInternal(childSqlInfo, parameterInfoList, resultObj);
						parentObject.addValue(resultObj);
						result = resultObj;
					}
					else {
						parentObject.addValue(null);
						result = null;
					}
				}
				else if(content.sqlInfo.getType() == SqlType.QueryMultiple) {
					Obj resultList = new Obj(content.objInfo, -1);
					ObjInfo objInfo = content.objInfo.toObjInfo(ObjType.Object);
					while(resultSet.next()) {
						Obj resultObj = new Obj(objInfo, objInfo.getChildCount());
						for(int col=1; col<=content.colCount; ++col) {
							ObjInfo childInfo = content.objInfo.getChild(col - 1);
							//resultObject.addValue(childResultInfo.getType().toObject(resultSet.getObject(col)));
							resultObj.addValue(getObjectResult(resultSet, col, childInfo.getType()));
						}
						resultList.addValue(resultObj);
						for(SqlInfo childSqlInfo : sqlInfo.getSqlInfoList())
							handleInternal(childSqlInfo, parameterInfoList, resultObj);
					}
					parentObject.addValue(resultList);
					result = resultList;
				}
			}
		} catch (SQLException e) {
			String message = String.format("Unable to execute query {%s}.", content.sqlInfo.getQuery());
			throw new WarningException(message, e);
		}
		

		if(resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				String message = String.format("Unable to execute query {%s}.", content.sqlInfo.getQuery());
				throw new WarningException(message, e);
			}
		}
		
		return new Pair<>(content.objInfo, result);
	}
	
	Object getJdbcResult(ResultSet resultSet, int col, ObjType type) throws SQLException {
		switch (type) {
		case String: 	return resultSet.getString(col);
		case Integer: 	return resultSet.getLong(col);
		case Decimal: 	return resultSet.getBigDecimal(col);
		case DateTime: 	return resultSet.getTimestamp(col);
		case Date: 		return resultSet.getDate(col);
		case Time: 		return resultSet.getTime(col);
		case Boolean: 	return resultSet.getBoolean(col);
		case ByteArray: return resultSet.getBytes(col);
		default:		return resultSet.getObject(col);
		}
	}
	
	Object getObjectResult(ResultSet resultSet, int col, ObjType type) throws SQLException {
		Object jdbcValue = getJdbcResult(resultSet, col, type);
		return jdbcValue == null ? null : type.toObject(jdbcValue);
	}

	void ensureResultInfo(ObjInfo parentInfo, SqlInfoContent content, ResultSet resultSet) throws BaseException {
		if(content.sqlInfo.getType() == SqlType.Update)
			return;
		if(content.objInfo != null) {
			//if(parentInfo != null && !parentInfo.hasChild(content.objInfo.getFieldName()))
			//	parentInfo.addChild(content.objInfo);
			return;
		}
		String fieldName = content.sqlInfo.getFieldName();
		content.objInfo = parentInfo == null ? null : parentInfo.getChild(fieldName);
		if(content.objInfo != null)
			return;

		ObjInfo objInfo = null;
		try {
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			if(content.sqlInfo.getType() == SqlType.QueryValue) {
				content.colCount = 1;
				ObjInfo columnInfo = content.sqlInfo.getColumn(fieldName);
				if(columnInfo == null) {
					objInfo = new ObjInfo(0, fieldName, ObjType.fromSqlType(resultSetMetaData.getColumnType(1)));
				}
				else {
					objInfo = new ObjInfo(0, fieldName, columnInfo.getType());
					objInfo.setExclude(columnInfo.getExclude());
				}
			}
			else {
				content.colCount = resultSetMetaData.getColumnCount();
				objInfo = new ObjInfo(
						content.colCount + content.sqlInfo.getChildCount(), 
						fieldName
				);
				objInfo.setTypeName(content.sqlInfo.getTypeName());
				if(content.sqlInfo.getType() == SqlType.QueryMultiple)
					objInfo.setType(ObjType.List);
				else if(content.sqlInfo.getType() == SqlType.QuerySingle)
					objInfo.setType(ObjType.Object);
				for(int col=1; col<=content.colCount; ++col) {
					String columnName = resultSetMetaData.getColumnName(col);
					//ObjType fieldType = null;
					ObjInfo childResultInfo = null;
					ObjInfo columnInfo = content.sqlInfo.getColumn(columnName);
					if(columnInfo == null) {
						childResultInfo = new ObjInfo(0, columnName, ObjType.fromSqlType(resultSetMetaData.getColumnType(col)));
					}
					else {
						childResultInfo = new ObjInfo(0, columnName, columnInfo.getType());
						childResultInfo.setExclude(columnInfo.getExclude());
					}
					objInfo.addChild(childResultInfo);
				}
			}
		} catch (SQLException e) {
			String message = String.format("Unable to execute query {%s}.", content.sqlInfo.getQuery());
			throw new WarningException(message, e);
		}
		
		if(parentInfo == null)
			objInfo.setIndex(0);
		else
			parentInfo.addChild(objInfo);

		content.objInfo = objInfo;
	}

	SqlInfoContent getSqlInfoContent(SqlInfo sqlInfo) throws BaseException {
		if(sqlInfo == null)
			return null;
		SqlInfoContent content = sqlInfoContentMap.get(sqlInfo);
		if(content == null) {
			content = createSqlInfoContent(sqlInfo);
			sqlInfoContentMap.put(sqlInfo, content);
		}
		return content;
	}
	
	SqlInfoContent createSqlInfoContent(SqlInfo sqlInfo) throws BaseException {
		SqlInfoContent content = new SqlInfoContent();
		content.sqlInfo = sqlInfo;
		
		Pair<String, List<Pair<QueryPartType, String>>> sqlInfoStatement = sqlInfo.createStatement();
		content.query = sqlInfoStatement.getFirst();
		content.argumentList = sqlInfoStatement.getSecond();
		
		try {
			content.statement = connection.prepareStatement(content.query);
		} catch (SQLException e) {
			String message = String.format(
					"Unable to execute query {%s}. Unable to create statement.", 
					sqlInfo.getQuery()
			);
			throw new WarningException(message, e);
		}

		return content;
	}
	
	static class SqlInfoContent {
		SqlInfo sqlInfo;
		
		String query;
		List<Pair<QueryPartType, String>> argumentList;

		PreparedStatement statement;
		//ResultSet resultSet;
		int colCount;
		
		ObjInfo objInfo;
	}
}
