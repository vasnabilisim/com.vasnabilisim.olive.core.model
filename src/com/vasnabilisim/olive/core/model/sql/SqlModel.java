package com.vasnabilisim.olive.core.model.sql;

import java.io.Serializable;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Model;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.Parameters;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlModel implements Model, Serializable {
	private static final long serialVersionUID = 137924639575268899L;

	protected SqlModelInfo sqlModelInfo = null;

	protected Obj rootObj = null;

	/**
	 * Info constructor.
	 * @param sqlModelInfo
	 */
	public SqlModel(SqlModelInfo sqlModelInfo) {
		this.sqlModelInfo = sqlModelInfo;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.Model#getName()
	 */
	@Override
	public String getName() {
		return sqlModelInfo.getName();
	}

	/**
	 * @see com.vasnabilisim.olive.core.model.Model#getInfo()
	 */
	@Override
	public SqlModelInfo getInfo() {
		return sqlModelInfo;
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.model.Model#getParameterInfoList()
	 */
	@Override
	public ObjInfoList getParameterInfoList() {
		return sqlModelInfo.getParameterInfoList();
	}
	
	@Override
	public ObjInfo getRootObjInfo() {
		return rootObj == null ? null : rootObj.getInfo();
	}
	
	@Override
	public Obj getRootObj() {
		return rootObj;
	}

	@Override
	public void initialize(Parameters parameters) throws BaseException {
		//Init process handled within create process. 
		//One day we may need to separate these. 
		//But not today. 06.09.2005 - MFG
		//Still not today 11.12.2013 - MFG
		//THE DAY FINALLY CAME 21.10.2014 - MFG
		rootObj = sqlModelInfo.handle(parameters);
	}
}
