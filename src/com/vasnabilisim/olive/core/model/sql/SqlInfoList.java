package com.vasnabilisim.olive.core.model.sql;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlInfoList extends LinkedList<SqlInfo> implements
		com.vasnabilisim.util.Cloneable<SqlInfoList>,
		java.lang.Cloneable, 
		Serializable 
{
	private static final long serialVersionUID = -7695990191851713141L;

	/**
	 * Default constructor.
	 */
	public SqlInfoList() {
		super();
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public SqlInfoList(SqlInfoList source) {
		super();
		for (SqlInfo sqlInfo : source)
			this.add(sqlInfo.cloneObject());
	}

	/**
	 * @see java.util.LinkedList#clone()
	 */
	@Override
	public Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public SqlInfoList cloneObject() {
		return new SqlInfoList(this);
	}
}
