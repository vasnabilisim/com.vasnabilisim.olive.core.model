package com.vasnabilisim.olive.core.model;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.statement.FieldStatement;
import com.vasnabilisim.olive.core.model.statement.Statement;

/**
 * Solves exact values of report fields. Used to navigate over models.
 * 
 * @author Menderes Fatih GUVEN
 */
public interface StatementSolver {

	/**
	 * Return the source report model.
	 * 
	 * @return
	 */
	Model getModel();

	/**
	 * Returns the value represented by given field statement.
	 * 
	 * @param fieldStatement
	 * @return
	 * @throws BaseException
	 */
	Object solveFieldStatement(FieldStatement fieldStatement) throws BaseException;

	/**
	 * Returns the value represented by given statement.
	 * 
	 * @param statement
	 * @return
	 * @throws BaseException
	 */
	Object solveStatement(Statement statement) throws BaseException;
}
