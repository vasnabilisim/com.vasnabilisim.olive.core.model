package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.WarningException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ObjInfoList implements 
		Iterable<ObjInfo>, 
		com.vasnabilisim.util.Cloneable<ObjInfoList>, 
		java.lang.Cloneable, 
		Serializable 
{
	private static final long serialVersionUID = -7575092563882153858L;

	TreeMap<String, ObjInfo> map;
	
	/**
	 * Default constructor.
	 */
	public ObjInfoList() {
		this.map = new TreeMap<>();
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public ObjInfoList(ObjInfoList source) {
		this.map = new TreeMap<>();
		for(Map.Entry<String, ObjInfo> entry : source.map.entrySet())
			this.map.put(entry.getKey(), entry.getValue().cloneObject());
	}
	
	@Override
	public Iterator<ObjInfo> iterator() {
		return map.values().iterator();
	}

	@Override
	public Object clone() {
		return cloneObject();
	}

	@Override
	public ObjInfoList cloneObject() {
		return new ObjInfoList(this);
	}
	
	/**
	 * Returns given named obj info. 
	 * Returns null if no obj info found with given name.
	 * @param name
	 * @return
	 */
	public ObjInfo getObjInfo(String name) {
		return map.get(name);
	}
	
	/**
	 * Adds given obj info. 
	 * @param objInfo
	 * @return
	 */
	public ObjInfo addObjInfo(ObjInfo objInfo) {
		return map.put(objInfo.getFieldName(), objInfo);
	}
	
	/**
	 * Removes given obj info.
	 * @param objInfo
	 * @return
	 */
	public ObjInfo removeObjInfo(ObjInfo objInfo) {
		return map.remove(objInfo.getFieldName());
	}
	
	/**
	 * Clones this and fills the clone from given value map.
	 * @param valueMap
	 * @param fillAll
	 * @return
	 * @throws BaseException
	 */
	public ObjInfoList cloneAndFill(Map<String, String> valueMap, boolean fillAll) throws BaseException {
		ObjInfoList clone = cloneObject();
		clone.fill(valueMap, fillAll);
		return clone;
	}

	/**
	 * Fills values from given map.
	 * @param valueMap
	 * @param fillAll
	 * @throws BaseException
	 */
	public void fill(Map<String, String> valueMap, boolean fillAll) throws BaseException {
		for(ObjInfo objInfo : map.values()) {
			if(!valueMap.containsKey(objInfo.getFieldName())) {
				if(fillAll)
					throw new WarningException(String.format("Unable to find value for [%s].", objInfo.getFieldName()));
				objInfo.setValue(null);
				continue;
			}
			String stringValue = valueMap.get(objInfo.getFieldName());
			Object value = objInfo.getType().fromStringToObject(stringValue);
			//Object jdbcValue = parameterInfo.getType().toJdbc(value);
			objInfo.setValue(value);
		}
	}

	/**
	 * Clones this and fills the clone from given parametersp.
	 * @param parameters
	 * @param fillAll
	 * @return
	 * @throws BaseException
	 */
	public ObjInfoList cloneAndFill(Parameters parameters, boolean fillAll) throws BaseException {
		ObjInfoList clone = cloneObject();
		clone.fill(parameters, fillAll);
		return clone;
	}

	/**
	 * Fills values from parameters.
	 * @param parameters
	 * @param fillAll
	 * @throws BaseException
	 */
	public void fill(Parameters parameters, boolean fillAll) throws BaseException {
		for(ObjInfo objInfo : map.values()) {
			if(!parameters.containsKey(objInfo.getFieldName())) {
				if(fillAll)
					throw new WarningException(String.format("Unable to find value for [%s].", objInfo.getFieldName()));
				objInfo.setValue(null);
				continue;
			}
			Object value = parameters.get(objInfo.getFieldName());
			//Object jdbcValue = parameterInfo.getType().toJdbc(value);
			objInfo.setValue(value);
		}
	}
	
	public int size() {
		return map.size();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}
	
	public ObjValue solve(String expression) throws BaseException {
		StringTokenizer tokenizer = new StringTokenizer(expression, ".");
		ObjType valueType = null;
		Object value = null;
		String tokens = "";
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if(tokens.length() > 0)
				tokens += '.';
			tokens += token;
			if(value == null) {
				ObjInfo objInfo = getObjInfo(token);
				if(objInfo == null)
					throw new WarningException(String.format("Unknown parameter [%s].", token));
				value = objInfo.getValue();
				valueType = objInfo.getType();
			} 
			else {
				if(!(value instanceof Obj))
					throw new WarningException(String.format("Unable to solve [%s]. Unable to access [%s].", expression, tokens));
				Obj obj = (Obj) value;
				ObjInfo childInfo = obj.getInfo().getChild(token);
				if(childInfo == null)
					throw new WarningException(String.format("Unable to solve [%s]. Unable to access [%s].", expression, tokens));
				value = obj.getValue(childInfo.getIndex());
				valueType = childInfo.getType();
			}
			if(value == null) {
				if(tokenizer.hasMoreTokens())
					valueType = null;
				break;
			}
			if(valueType == ObjType.List && tokenizer.hasMoreTokens()) {
				Obj obj = (Obj)value;
				value = obj.current();
				valueType = ObjType.Object;
			}
		}
		return new ObjValue(value, valueType);
	}
}
