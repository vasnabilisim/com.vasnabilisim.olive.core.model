package com.vasnabilisim.olive.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.WarningException;
import com.vasnabilisim.util.Iterative;

/**
 * @author Menderes Fatih GUVEN
 */
public class Obj implements Iterative<Object>, Serializable {
	private static final long serialVersionUID = 8675299252378632531L;

	Obj parent;
	ObjInfo objInfo;
	ArrayList<Object> values;
	
	public Obj(ObjInfo objInfo) {
		this(objInfo, objInfo.getChildCount());
	}
	
	public Obj(ObjInfo objInfo, int size) {
		this.objInfo = objInfo;
		if(size < 0)
			values = new ArrayList<>(20);
		else
			values = new ArrayList<>(size);
	}
	
	public void fillNull() {
		while(values.size() < objInfo.getChildCount())
			values.add(null);
	}
	
	public Obj getParent() {
		return parent;
	}
	
	@Override
	public String toString() {
		if(objInfo.getType() == ObjType.List)
			return "[" + objInfo + ", " + values.size() + "]";
		return "{" + objInfo + ", " + values + "}";
	}

	public ObjInfo getInfo() {
		return objInfo;
	}
	
	public int getValueCount() {
		return values.size();
	}

	public ArrayList<Object> getValues() {
		return values;
	}
	
	public Object getFirstValue() {
		if(values.isEmpty())
			return null;
		return values.get(0);
	}
	
	public Object getLastValue() {
		if(values.isEmpty())
			return null;
		return values.get(values.size() - 1);
	}
	
	public Object getValue(int index) {
		if(index >= values.size())
			return null;
		return values.get(index);
	}
	
	public void setValue(int index, Object value) {
		if(value instanceof Obj) {
			Obj child = (Obj) value;
			child.parent = this;
		}
		while(index >= values.size())
			values.add(null);
		values.set(index, value);
	}
	
	public void addValue(Object value) {
		if(value instanceof Obj) {
			Obj child = (Obj) value;
			child.parent = this;
		}
		values.add(value);
	}
	
	public Object getValue(String childName) {
		ObjInfo childResultInfo = objInfo.getChild(childName);
		return values.get(childResultInfo.index);
	}
	
	public ObjValue solve(String expression) throws BaseException {
		StringTokenizer tokenizer = new StringTokenizer(expression, ".", false);
		String tokens = "";
		ObjType valueType = null;
		Object value = this;
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			tokens += token;
			if(!(value instanceof Obj))
				throw new WarningException(String.format("Unable to solve [%s]. Unable to access [%s].", expression, tokens));
			Obj obj = (Obj) value;
			if("^".equals(token)) 
				value = obj.parent;
			else {
				ObjInfo childInfo = obj.getInfo().getChild(token);
				if(childInfo == null)
					throw new WarningException(String.format("Unable to solve [%s]. Unable to access [%s].", expression, tokens));
				value = obj.getValue(childInfo.getIndex());
				valueType = childInfo.getType();
			}
			if(value == null) {
				if(tokenizer.hasMoreTokens())
					valueType = null;
				break;
			}
			if(valueType == ObjType.List && tokenizer.hasMoreTokens()) {
				obj = (Obj) obj.current();
				valueType = ObjType.Object;
			}
		}
		return new ObjValue(value, valueType);
	}

	private Iterator<Object> iterator = null;
	private Object current = null;

	@Override
	public void start() {
		iterator = values.iterator();
		current = null;
	}

	@Override
	public void stop() {
		iterator = null;
		current = null;
	}

	@Override
	public Object current() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		return current;
	}

	@Override
	public boolean hasNext() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		return iterator.hasNext();
	}

	@Override
	public Object next() {
		if(iterator == null)
			throw new IllegalStateException("Iterative not started.");
		current = iterator.next();
		return current;
	}
}
