package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Holds only one sub node.
 * 
 * @author Menderes Fatih GUVEN
 */
public class IdentityNode extends Node implements Serializable {
	private static final long serialVersionUID = 4459401785660881264L;

	protected Node node = null;

	/**
	 * Default constructor.
	 */
	public IdentityNode() {
		super();
	}

	@Override
	public void setNode(Node node) {
		this.node = node;
	}

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		return node.evaluate(solver);
	}

}
