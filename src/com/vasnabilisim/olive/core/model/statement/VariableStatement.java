package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.util.StringTokenizer;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Used to combine dynamic report items to real report object fields.
 * 
 * @author Menderes Fatih GÜVEN
 */
public class VariableStatement extends Statement implements Serializable {
	private static final long serialVersionUID = -7232486751947762261L;

	/**
	 * Type of the statement.
	 */
	private ObjType type = ObjType.Other;

	/**
	 * Type-Value constructor.
	 * 
	 * @param type
	 * @param statement
	 */
	public VariableStatement(ObjType type, String statement) {
		super(statement);
		this.type = type;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public VariableStatement(VariableStatement source) {
		super(source);
		this.type = source.type;
	}

	@Override
	public String toString() {
		return "$V(" + getText() + ")";
	}

	@Override
	public VariableStatement cloneObject() {
		return new VariableStatement(this);
	}

	/**
	 * Returns all tokens of the statement.
	 * 
	 * @return java.lang.String[]
	 */
	public String[] getTokens() {
		StringTokenizer st = new StringTokenizer(text, ".", false);
		String[] arrStatement = new String[st.countTokens()];
		int index = 0;
		while (st.hasMoreTokens())
			arrStatement[index++] = st.nextToken();
		return arrStatement;
	}

	@Override
	public ObjType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 */
	public void setType(ObjType type) {
		this.type = type;
	}
}
