package com.vasnabilisim.olive.core.model.statement;

import java.util.LinkedList;
import java.util.StringTokenizer;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.Model;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Parses buffer type statements. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class BufferStatementParser {

	/**
	 * Parses the given source and returns a buffer statement.
	 * @param source
	 * @param model
	 * @return
	 * @throws BaseException
	 */
	public static BufferStatement parse(String source, Model reportModel) throws BaseException {
		BufferStatementParser parser = new BufferStatementParser(source, reportModel);
		return parser.parseStament();
	}

	private String source = null;
	private Model model = null;
	
	private BufferStatementParser(String source, Model reportModel) {
		this.source = source;
		this.model = reportModel;
	}
	
	/**
	 * Parses the source and returns an buffer statement.
	 * @return
	 * @throws BaseException
	 */
	private BufferStatement parseStament() throws BaseException {
		StringTokenizer tokenizer = new StringTokenizer(source, "$)", true);
		LinkedList<Statement> statementList = new LinkedList<Statement>();
		String token1 = null;
		String token2 = null;
		String token3 = null;
		StringBuilder builder = new StringBuilder(this.source.length());
		while(tokenizer.hasMoreTokens()) {
			token1 = tokenizer.nextToken();
			if("$".equals(token1)) {
				if(tokenizer.hasMoreTokens()) {
					token2 = tokenizer.nextToken();
					if(tokenizer.hasMoreTokens()) {
						token3 = tokenizer.nextToken();
						if(token2.toUpperCase().startsWith("P(") && token3.equals(")")) {
							if(builder.length() > 0) {
								statementList.add(new StringStatement(builder.toString()));
								builder = new StringBuilder(this.source.length());
							}
							String parameter = token2.substring(2, token2.length()).trim();
							statementList.add(new ParameterStatement(getParameterType(parameter), parameter));
						}//if(token2.startsWith("P(") && token3.equals(")"))
						else if(token2.toUpperCase().startsWith("V(") && token3.equals(")")) {
							if(builder.length() > 0) {
								statementList.add(new StringStatement(builder.toString()));
								builder = new StringBuilder(this.source.length());
							}
							String variable = token2.substring(2, token2.length()).trim();
							statementList.add(new VariableStatement(getVariableType(variable), variable));
						}//if(token2.startsWith("P(") && token3.equals(")"))
						else if(token2.toUpperCase().startsWith("E(") && token3.equals(")")) {
							if(builder.length() > 0) {
								statementList.add(new StringStatement(builder.toString()));
								builder = new StringBuilder(this.source.length());
							}
							String equation = token2.substring(2, token2.length()).trim();
							statementList.add(EquationStatementParser.parse(equation, model));
						}//if(token2.startsWith("P(") && token3.equals(")"))
						else
							builder.append(token1).append(token2).append(token3);
					}//if(tokenizer.hasMoreTokens())
					else
						builder.append(token1).append(token2);
				}//if(tokenizer.hasMoreTokens())
				else
					builder.append(token1);
			}//if("$".equals(token1))
			else
				builder.append(token1);
		}//while(tokenizer.hasMoreTokens())
		statementList.add(new StringStatement(builder.toString()));

		return new BufferStatement(this.source, statementList);
	}

	/**
	 * Returns the type of the variable of given source. 
	 * Also validate the existence of the variable.
	 * @param source
	 * @throws BaseException
	 */
	private ObjType getVariableType(String source) throws BaseException {
		if(model == null)
			return ObjType.Other;
		ObjInfo objInfo = model.getRootObjInfo().solve(source);
		if(objInfo == null) {
			String message = String.format("Unable to solve variable %s", source);
			throw new ErrorException(message);
		}
		return objInfo.getType();
	}
	
	/**
	 * Returns the type of the parameter of given source. 
	 * Also validate the existence of the parameter.
	 * @param source
	 * @throws BaseException
	 */
	private ObjType getParameterType(String source) throws BaseException {
		if(model == null)
			return ObjType.Other;
		ObjInfoList parameterInfoList = model.getParameterInfoList();
		if(parameterInfoList == null || parameterInfoList.isEmpty()) {
			String message = String.format("Unable to solve parameter %s", source);
			throw new ErrorException(message);
		}
		for(ObjInfo parameterInfo : parameterInfoList) {
			if(source.equalsIgnoreCase(parameterInfo.getFieldName()))
				return parameterInfo.getType();
		}
		String message = String.format("Unable to solve parameter %s", source);
		throw new ErrorException(message);
	}
}
