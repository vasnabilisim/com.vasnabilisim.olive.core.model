package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Unary minus operator type.
 * 
 * @author Menderes Fatih GUVEN
 */
public class UnaryMinusOperatorNode extends UnaryOperatorNode implements Serializable {
	private static final long serialVersionUID = -974873858101572669L;

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		BigDecimal result = node.evaluate(solver);
		return result.negate();
	}

}
