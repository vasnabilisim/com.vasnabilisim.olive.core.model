package com.vasnabilisim.olive.core.model.statement;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.text.NumberFormatter;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.Model;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Equation statement parser. 
 * Parses equation type statements.
 * 
 * @author Menderes Fatih GUVEN
 */
public class EquationStatementParser {

	/**
	 * Parses the given source and returns a equation statement.
	 * @param source
	 * @param reportModel
	 * @return
	 * @throws BaseException
	 */
	public static EquationStatement parse(String source, Model reportModel) throws BaseException {
		EquationStatementParser parser = new EquationStatementParser(source, reportModel);
		return parser.parseStament();
	}

	private String source = null;
	private Model reportModel = null;
	
	/**
	 * Default constructor.
	 * @param source
	 */
	private EquationStatementParser(String source, Model reportModel) {
		this.source = source;
		this.reportModel = reportModel;
	}
	
	/**
	 * Parses the source and returns an equation statement.
	 * @return
	 * @throws BaseException
	 */
	private EquationStatement parseStament() throws BaseException {
		String source = this.source.replaceAll("\\s","");
		IdentityNode rootNode = new IdentityNode();
		construct(rootNode, source);
		EquationStatement statement = new EquationStatement(this.source, rootNode);
		return statement;
	}
	
	/**
	 * Construct a new node and adds to given parent. 
	 * Which node to be constructed, decided by given source.
	 * @param parent
	 * @param source
	 * @throws BaseException
	 */
	private void construct(Node parent, String source) throws BaseException {
		int index = 0;
		if( (index = indexOf(source, '+')) != -1) {
			if(index == 0)
				constructUnaryOperator("+", parent, source);
			else
				constructBinaryOperator("+", parent, source, index);
		}
		else if( (index = indexOf(source, '-')) != -1) {
			if(index == 0)
				constructUnaryOperator("-", parent, source);
			else
				constructBinaryOperator("-", parent, source, index);
		}
		else if( (index = indexOf(source, '*')) != -1) {
			int index2 = indexOf(source, '/');
			if(index > index2)
				constructBinaryOperator("*", parent, source, index);
			else
				constructBinaryOperator("/", parent, source, index2);
		}
		else if( (index = indexOf(source, '/')) != -1)
			constructBinaryOperator("/", parent, source, index);
		else if( isParanthesis(source) )
			construct(parent, source.substring(1, source.length()-1));
		else if( isConstant(source) )
			constructConstant(parent, source);
		else if( isVariable(source) )
			constructVariable(parent, source.substring(3, source.length()-1));
		else if( isParameter(source) )
			constructParameter(parent, source.substring(3, source.length()-1));
		else {
			String message = String.format("Unable to parse statement %s", source);
			throw new ErrorException(message);
		}
	}
	
	private NumberFormatter numberFormatter = null;
	
	private NumberFormatter getNumberFormater() {
		if(numberFormatter != null)
			return numberFormatter;
		Locale locale = Locale.getDefault();
		NumberFormat format = null;
		if(locale == null)
			format = NumberFormat.getNumberInstance();
		else
			format = NumberFormat.getNumberInstance(locale);
		format.setGroupingUsed(false);
		numberFormatter = new NumberFormatter(format);
		numberFormatter.setValueClass(BigDecimal.class);
		return numberFormatter;
	}
	
	/**
	 * Is given source a variable.
	 * @param source
	 * @return
	 * @throws BaseException
	 */
	private boolean isVariable(String source) throws BaseException{
		if(source.length() < 5)
			return false;
		if(	source.charAt(0) == '$' && 
			(source.charAt(1) == 'V' || source.charAt(1) == 'v') && 
			source.charAt(2) == '(' && 
			source.charAt(source.length()-1) == ')'
		)
		{
			//From now on it must be a variable. 
			//Throw exception if fails.
			String variableSource = source.substring(3, source.length()-1);
			if(reportModel != null)
				validateVariable(variableSource);
			return true;
		}
		return false;
	}

	/**
	 * Does variable exists in the report model.
	 * @param source
	 * @throws BaseException
	 */
	private void validateVariable(String source) throws BaseException {
		ObjInfo objInfo = reportModel.getRootObjInfo().solve(source);
		if(objInfo.getType() != ObjType.Integer || objInfo.getType() != ObjType.Decimal) {
			String message = String.format("Variable %s is not of number type.", source);
			throw new ErrorException(message);
		}
	}
	
	/**
	 * Is given source a parameter.
	 * @param source
	 * @return
	 * @throws BaseException
	 */
	private boolean isParameter(String source) throws BaseException {
		if(source.length() < 5)
			return false;
		if(	source.charAt(0) == '$' && 
			(source.charAt(1) == 'P' || source.charAt(1) == 'p') && 
			source.charAt(2) == '(' && 
			source.charAt(source.length()-1) == ')'
		)
		{
			//From now on it must be a parameter. 
			//Throw exception if fails.
			String parameterSource = source.substring(3, source.length()-1);
			if(reportModel != null)
				validateParameter(parameterSource);
			return true;
		}
		return false;
	}
	
	/**
	 * Does parameter exists in the report model.
	 * @param source
	 * @throws BaseException
	 */
	private void validateParameter(String source) throws BaseException {
		ObjInfoList parameterInfoList = reportModel.getParameterInfoList();
		if(parameterInfoList == null || parameterInfoList.isEmpty()) {
			String message = String.format("Unable to solve parameter %s", source);
			throw new ErrorException(message);
		}
		for(ObjInfo parameterInfo : parameterInfoList) {
			if(source.equalsIgnoreCase(parameterInfo.getFieldName())) {
				ObjType type = parameterInfo.getType();
				if(	type == ObjType.Integer || type == ObjType.Decimal)
					return;
				String message = String.format("Parameter %s is not of number type.", source);
				throw new ErrorException(message);
			}
		}
		String message = String.format("Unable to solve parameter %s", source);
		throw new ErrorException(message);
	}

	/**
	 * Is given source a decimal value;
	 * @param source
	 * @return
	 */
	private boolean isConstant(String source) {
		try {
			getNumberFormater().stringToValue(source);
			return true;
		}
		catch(ParseException e) {
			return false;
		}
	}

	/**
	 * Is given source a parenthesis statement.
	 * @param source
	 * @return
	 */
	private boolean isParanthesis(String source) {
		return
			source.length() >= 2 
				&&
			source.charAt(0) == '(' 
				&& 
			source.charAt(source.length()-1) == ')';
	}

	/**
	 * Constructs a unary operator node and adds to given parent. 
	 * @param operator
	 * @param parent
	 * @param source
	 * @throws BaseException
	 */
	private void constructUnaryOperator(String operator, Node parent, String source) throws BaseException{
		UnaryOperatorNode node = null;
		if("+".equals(operator))
			node = new UnaryPlusOperatorNode();
		else if("-".equals(operator))
			node = new UnaryMinusOperatorNode();
		else {
			String message = String.format("Un identified unary operator %s in %s", operator, source);
			throw new ErrorException(message);
		}
		
		String operantSource = source.substring(1);
		construct(node, operantSource);
		parent.setNode(node);
	}

	/**
	 * Constructs a unary operator node and adds to given parent. 
	 * @param operator
	 * @param parent
	 * @param source
	 * @param index
	 * @throws BaseException
	 */
	private void constructBinaryOperator(String operator, Node parent, String source, int index) throws BaseException {
		BinaryOperatorNode node = null;
		if("+".equals(operator))
			node = new BinaryPlusOperatorNode();
		else if("-".equals(operator))
			node = new BinaryMinusOperatorNode();
		else if("*".equals(operator))
			node = new BinaryMultiplicationOperatorNode();
		else if("/".equals(operator))
			node = new BinaryDivisionOperatorNode();
		else {
			String message = String.format("Un identified binary operator %s in %s", operator, source);
			throw new ErrorException(message);
		}
		
		String leftOperantSource = source.substring(0, index);
		String rightOperantSource = source.substring(index+1);
		construct(node, leftOperantSource);
		construct(node, rightOperantSource);
		parent.setNode(node);
	}
	
	/**
	 * Constructs a constant node and adds to given parent.
	 * @param parent
	 * @param source
	 * @throws BaseException
	 */
	private void constructConstant(Node parent, String source) throws BaseException {
		BigDecimal value = null;
		try {
			value = (BigDecimal) getNumberFormater().stringToValue(source);
		}
		catch(ParseException e) {
			String message = String.format("Invalid constant %s", source);
			throw new ErrorException(message);
		}
		
		ConstantNode node = new ConstantNode(value);
		parent.setNode(node);
	}
	
	/**
	 * Constructs a variable statement node and adds to given parent.
	 * @param parent
	 * @param source
	 */
	private void constructVariable(Node parent, String source) {
		VariableStatement statement = new VariableStatement(ObjType.Decimal, source);
		StatementNode node = new StatementNode(statement);
		parent.setNode(node);
	}
	
	/**
	 * Constructs a parameter statement node and adds to given parent.
	 * @param parent
	 * @param source
	 */
	private void constructParameter(Node parent, String source) {
		ParameterStatement statement = new ParameterStatement(ObjType.Decimal, source);
		StatementNode node = new StatementNode(statement);
		parent.setNode(node);
	}
	
	/**
	 * Returns the last index of given target in given source which is not in parenthesis. 
	 * @param source
	 * @param target
	 * @return
	 */
	private int indexOf(String source, char target) {
		char ch = '@';
		int paranthesisCount = 0;
		for(int index=source.length()-1; index>=0; --index) {
			ch = source.charAt(index);
			if(ch == ')')
				++paranthesisCount;
			else if(ch == '(')
				--paranthesisCount;
			else if(ch == target && paranthesisCount == 0)
				return index;
		}
		return -1;
	}

}
