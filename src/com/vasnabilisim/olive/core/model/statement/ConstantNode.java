package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Holds a decimal value.
 * 
 * @author Menderes Fatih GUVEN
 */
public class ConstantNode extends Node implements Serializable {
	private static final long serialVersionUID = 328069104165314173L;

	protected BigDecimal value = BigDecimal.ZERO;
	
	/**
	 * Value constructor.
	 * @param value
	 */
	public ConstantNode(BigDecimal value) {
		this.value = value;
	}

	@Override
	public void setNode(Node node) {
		//Absolute for this type.
	}

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		return value;
	}

}
