package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

/**
 * Abstract base class of binary operator nodes. 
 * These types have two sub nodes.
 * 
 * @author Menderes Fatih GÜVEN
 */
public abstract class BinaryOperatorNode extends Node implements Serializable {
	private static final long serialVersionUID = -6418700718096584139L;

	protected Node leftNode = null;
	protected Node rightNode = null;

	public void setNode(Node node) {
		if(leftNode == null)
			leftNode = node;
		else
			rightNode = node;
	}

}
