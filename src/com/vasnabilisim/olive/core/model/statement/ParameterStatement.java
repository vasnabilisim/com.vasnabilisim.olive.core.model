package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Used to combine dynamic report items to real report parameters.
 * 
 * @author Menderes Fatih GUVEN
 */
public class ParameterStatement extends Statement implements Serializable {
	private static final long serialVersionUID = -8164080962614588551L;

	/**
	 * Type of the statement.
	 */
	private ObjType type = ObjType.Other; 
	
	/**
	 * Value constructor.
	 * @param parameterInfo
	 */
	public ParameterStatement(ObjType type, String statement) {
		super(statement);
		this.type = type;
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public ParameterStatement(ParameterStatement source) {
		super(source);
		this.type = source.type;
	}
	
	@Override
	public String toString() {
		return "$P(" + getText() + ")";
	}
	
	@Override
	public ParameterStatement cloneObject() {
		return new ParameterStatement(this);
	}

	@Override
	public ObjType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * @param type
	 */
	public void setType(ObjType type) {
		this.type = type;
	}
}
