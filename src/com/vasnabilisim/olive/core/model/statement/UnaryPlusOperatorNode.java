package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Unary plus operator type.
 * 
 * @author Menderes Fatih GUVEN
 */
public class UnaryPlusOperatorNode extends UnaryOperatorNode implements Serializable {
	private static final long serialVersionUID = 7433216602514558222L;

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		return node.evaluate(solver);
	}
}
