package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Abstract base class of all nodes of statement tree.
 * 
 * @author Menderes Fatih GÜVEN
 */
public abstract class Node implements Serializable {
	private static final long serialVersionUID = -7743204326877469578L;

	/**
	 * Sets sub nodes. This method may called iteratively to set all sub nodes.
	 * First call is for the 0 indexed sub node.
	 * 
	 * @param node
	 */
	public abstract void setNode(Node node);

	/**
	 * Evaluates the value of node by solving dynamics from given solver.
	 * 
	 * @param solver
	 * @return
	 * @throws BaseException
	 */
	public abstract BigDecimal evaluate(StatementSolver solver) throws BaseException;
}
