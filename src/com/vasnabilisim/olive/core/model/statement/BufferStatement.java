package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.util.List;

import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.util.CloneableArrayList;

/**
 * Buffer statement class.
 * Buffer statements are ordered statements. 
 * Variable, parameter and equation statements can be used in buffer statements.
 * Used to combine dynamic report components to real report object fields. 
 * Obj of the statement is of string type. 
 * Not string statements are converted to string. 
 * $V(Invoice.Price) : example of a variable statement. 
 * $P(InvoiceId) : example of a parameter statement. 
 * $E($V(Invoice.Price) * $V(Invoice.Quantity)) : example of a equation statement. 
 * Total price of the good is $E($V(Invoice.Price) * $V(Invoice.Quantity)) : example of a buffer statement.
 *  
 * @author Menderes Fatih GUVEN
 */
public class BufferStatement extends Statement implements Serializable {
	private static final long serialVersionUID = 4603372594931813444L;

	private CloneableArrayList<Statement> statementList = null;
	
	/**
	 * Value constructor.
	 * @param statement
	 */
	public BufferStatement(String statement, List<Statement> statementList) {
		super(statement);
		this.statementList = new CloneableArrayList<>(statementList);
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public BufferStatement(BufferStatement source) {
		super(source);
		this.statementList = source.statementList.cloneObject();
	}

	@Override
	public BufferStatement cloneObject() {
		return new BufferStatement(this);
	}

	@Override
	public ObjType getType() {
		return ObjType.String;
	}

	/**
	 * Returns the list of statements.
	 * @return
	 */
	public List<Statement> getStatementList() {
		return statementList;
	}
}
