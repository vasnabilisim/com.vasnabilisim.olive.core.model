package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Used to combine statements to report items as property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class FieldStatement implements
		com.vasnabilisim.util.Cloneable<FieldStatement>,
		java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = -6726089322513453361L;

	/**
	 * Empty field statement.
	 */
	public static final FieldStatement Empty = new FieldStatement((Statement)null);

	/**
	 * Statement to be looked in report object.
	 */
	protected Statement statement = null;

	/**
	 * Type-Value constructor.
	 * 
	 * @param statement
	 */
	public FieldStatement(Statement statement) {
		this.statement = statement;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public FieldStatement(FieldStatement source) {
		this.statement = source.statement.cloneObject();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return statement == null ? "" : statement.toString();
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() {
		return cloneObject();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public FieldStatement cloneObject() {
		return new FieldStatement(this);
	}

	/**
	 * Returns the statement.
	 * 
	 * @return
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * Sets the statement.
	 * 
	 * @param statement
	 */
	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	/**
	 * Returns the type.
	 * 
	 * @return
	 */
	public ObjType getType() {
		return statement == null ? ObjType.Other : statement.getType();
	}
}
