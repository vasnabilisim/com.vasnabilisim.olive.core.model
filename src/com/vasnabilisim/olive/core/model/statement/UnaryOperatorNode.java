package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

/**
 * Abstract base class of unary operator nodes. These types have only one sub
 * node.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class UnaryOperatorNode extends Node implements Serializable {
	private static final long serialVersionUID = 6907829100610199226L;

	protected Node node = null;

	@Override
	public void setNode(Node node) {
		this.node = node;
	}
}
