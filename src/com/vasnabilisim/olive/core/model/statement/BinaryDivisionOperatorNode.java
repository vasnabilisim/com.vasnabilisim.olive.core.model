package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Binary division operator type.
 * 
 * @author Menderes Fatih GUVEN
 */
public class BinaryDivisionOperatorNode extends BinaryOperatorNode implements Serializable {
	private static final long serialVersionUID = 8249573028214034320L;

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		BigDecimal leftValue = leftNode.evaluate(solver);
		BigDecimal rightValue = rightNode.evaluate(solver);
		BigDecimal result = leftValue.divide(rightValue);
		return result;
	}
}
