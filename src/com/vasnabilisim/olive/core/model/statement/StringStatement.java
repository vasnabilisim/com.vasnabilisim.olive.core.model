package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Value of the statement is always statement itself.
 * 
 * @author Menderes Fatih GUVEN
 */
public class StringStatement extends Statement implements Serializable {
	private static final long serialVersionUID = 8326190676727255301L;

	/**
	 * Value constructor.
	 * 
	 * @param statement
	 */
	public StringStatement(String statement) {
		super(statement);
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public StringStatement(StringStatement source) {
		super(source);
	}

	@Override
	public StringStatement cloneObject() {
		return new StringStatement(this);
	}

	@Override
	public ObjType getType() {
		return ObjType.String;
	}

}
