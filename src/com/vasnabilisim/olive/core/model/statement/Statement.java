package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Statement class. Used to combine dynamic report items to object fields.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class Statement implements Serializable,
		com.vasnabilisim.util.Cloneable<Statement>, 
		java.lang.Cloneable 
{
	private static final long serialVersionUID = -8677208368706735974L;

	/**
	 * Statement to be looked in report object.
	 */
	protected String text = null;

	/**
	 * Value constructor.
	 * 
	 * @param text
	 */
	protected Statement(String text) {
		this.text = text;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	protected Statement(Statement source) {
		this.text = source.text;
	}

	/**
	 * Returns full statement string.
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the statement.
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Returns the type of the statement.
	 * 
	 * @return
	 */
	public abstract ObjType getType();

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getText();
	}

	/**
	 * @see com.bilgibiz.bizreport.core.Cloneable#cloneObject()
	 */
	@Override
	public abstract Statement cloneObject();
}
