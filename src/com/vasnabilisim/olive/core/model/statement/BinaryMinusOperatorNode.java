package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Binary minus operator type.
 * 
 * @author Menderes Fatih GUVEN
 */
public class BinaryMinusOperatorNode extends BinaryOperatorNode implements Serializable {
	private static final long serialVersionUID = 2780870620916432386L;

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		BigDecimal leftValue = leftNode.evaluate(solver);
		BigDecimal rightValue = rightNode.evaluate(solver);
		BigDecimal result = leftValue.subtract(rightValue);
		return result;
	}
}
