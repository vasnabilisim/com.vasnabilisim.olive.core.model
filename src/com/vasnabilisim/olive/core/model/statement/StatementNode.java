package com.vasnabilisim.olive.core.model.statement;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Holds a statement. Has no sub nodes.
 * 
 * @author Menderes Fatih GUVEN
 */
public class StatementNode extends Node {
	private static final long serialVersionUID = -6507118021953214509L;

	protected Statement statement = null;

	/**
	 * Statement constructor.
	 * 
	 * @param statement
	 */
	public StatementNode(Statement statement) {
		this.statement = statement;
	}

	@Override
	public void setNode(Node node) {
		// Absolute for this type.
	}

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		Object value = solver.solveStatement(statement);
		if( value instanceof BigDecimal) {
			BigDecimal number = (BigDecimal) value;
			return number;
		}
		if( value instanceof BigInteger) {
			BigInteger number = (BigInteger) value;
			return new BigDecimal(number);
		}
		if (value instanceof Number) {
			Number number = (Number) value;
			return new BigDecimal(number.doubleValue());
		}
		String message = String.format("Not a number statement:[%s]", statement);
		throw new ErrorException(message);
	}
}
