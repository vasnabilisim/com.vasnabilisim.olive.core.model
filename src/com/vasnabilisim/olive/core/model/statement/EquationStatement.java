package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;

import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Equation statements are structural operations of simple number statements. 
 * Simple statements are variable and parameter statements.
 * Used to combine dynamic report components to real report object fields. 
 * unary +, - and binary +, -, *, / operations can be used. 
 * Obj of the statement must be of number type. 
 * Do not use any field but number in the equation. 
 * Constants and parenthesis can also be used in the equation. 
 * $V(Invoice.Price) : example of a variable statement. 
 * $P(InvoiceId) : example of a parameter statement. 
 * $E($V(Invoice.Price) * $V(Invoice.Quantity)) : example of a equation statement. 
 * 
 * @author Menderes Fatih GUVEN
 */
public class EquationStatement extends Statement implements Serializable {
	private static final long serialVersionUID = -2183331558070571968L;

	private Node rootNode = null;
	
	/**
	 * Value constructor.
	 * @param statement
	 * @param rootNode
	 */
	public EquationStatement(String statement, Node rootNode) {
		super(statement);
		this.rootNode = rootNode;
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public EquationStatement(EquationStatement source) {
		super(source);
		this.rootNode = source.rootNode;
	}

	@Override
	public ObjType getType() {
		return ObjType.Decimal;
	}
	
	@Override
	public String toString() {
		return "$E(" + getText() + ")";
	}
	
	@Override
	public EquationStatement cloneObject() {
		return new EquationStatement(this);
	}

	/**
	 * Returns the root node.
	 * @return
	 */
	public Node getRootNode() {
		return rootNode;
	}

	/**
	 * Sets the root node.
	 * @param rootNode The rootNode to set.
	 */
	public void setRootNode(Node rootNode) {
		this.rootNode = rootNode;
	}
}
