package com.vasnabilisim.olive.core.model.statement;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.StatementSolver;

/**
 * Binary multiplication operator type.
 * 
 * @author Menderes Fatih GUVEN
 */
public class BinaryMultiplicationOperatorNode extends BinaryOperatorNode implements Serializable {
	private static final long serialVersionUID = 4717120924316673930L;

	@Override
	public BigDecimal evaluate(StatementSolver solver) throws BaseException {
		BigDecimal leftValue = leftNode.evaluate(solver);
		BigDecimal rightValue = rightNode.evaluate(solver);
		BigDecimal result = leftValue.multiply(rightValue);
		return result;
	}
}
