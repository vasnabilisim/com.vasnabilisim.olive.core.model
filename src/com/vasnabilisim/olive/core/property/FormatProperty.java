package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.model.Format;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * Format Property.
 * @author Menderes Fatih GUVEN
 */
public class FormatProperty extends AbstractProperty<Format> {
	private static final long serialVersionUID = -3945653055911664752L;

	public FormatProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public FormatProperty(String group, String alias, String name, String description, Format value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public FormatProperty(FormatProperty source) {
		super(source);
	}

	@Override
	public FormatProperty clone() {
		return new FormatProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Format";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Format> getValueType() {
		return Format.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		value = stringValue == null ? null : Format.valueOf(stringValue);
	}
	
	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "Type", value.getType().name());
			createXmlElement(document, element, "Pattern", value.getPattern());
		}
	}

	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		ObjType type = readXmlElement(document, element, "Type", ObjType.class,  null);
		String pattern = readXmlElement(document, element, "Pattern", (String)null);
		if(type == null || pattern == null)
			value = null;
		else
			value = new Format(type, pattern);
	}
}
