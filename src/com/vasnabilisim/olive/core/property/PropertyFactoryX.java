package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.model.Format;
import com.vasnabilisim.olive.core.model.statement.FieldStatement;

/**
 * Property factory class. 
 * This type is singleton. 
 * Use getInstance method to get the only instance of this type.
 * @author Menderes Fatih GUVEN
 */
public class PropertyFactoryX extends PropertyFactory {

	/**
	 * One and only instance.
	 */
	protected static PropertyFactoryX instance = new PropertyFactoryX();
	
	/**
	 * Returns the instance.
	 * @return
	 */
	public static PropertyFactoryX getInstance() {
		return instance;
	}
	
	/**
	 * Sets the instance.
	 * @param instance
	 */
	public static void setInstance(PropertyFactoryX instance) {
		PropertyFactoryX.instance = instance;
	}
	
	protected PropertyFactoryX() {
		register("FieldStatement", FieldStatementProperty.class);
		register("Format", FormatProperty.class);
	}
	
	public static FieldStatementProperty createFieldStatementProperty(String group, String title, String name, String description, FieldStatement value) {
		return new FieldStatementProperty(group, title, name, description, value);
	}

	public static FormatProperty createFormatProperty(String group, String title, String name, String description, Format value) {
		return new FormatProperty(group, title, name, description, value);
	}
}
