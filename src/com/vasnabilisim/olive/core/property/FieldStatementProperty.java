package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.core.model.statement.BufferStatement;
import com.vasnabilisim.olive.core.model.statement.BufferStatementParser;
import com.vasnabilisim.olive.core.model.statement.EquationStatement;
import com.vasnabilisim.olive.core.model.statement.EquationStatementParser;
import com.vasnabilisim.olive.core.model.statement.FieldStatement;
import com.vasnabilisim.olive.core.model.statement.ParameterStatement;
import com.vasnabilisim.olive.core.model.statement.Statement;
import com.vasnabilisim.olive.core.model.statement.StringStatement;
import com.vasnabilisim.olive.core.model.statement.VariableStatement;
import com.vasnabilisim.util.StringUtil;


/**
 * Field Statement Property.
 * @author Menderes Fatih GUVEN
 */
public class FieldStatementProperty extends AbstractProperty<FieldStatement> {
	private static final long serialVersionUID = 8898476993277713657L;

	public FieldStatementProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public FieldStatementProperty(String group, String alias, String name, String description, FieldStatement value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public FieldStatementProperty(FieldStatementProperty source) {
		super(source);
	}

	@Override
	public FieldStatementProperty clone() {
		return new FieldStatementProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "FieldStatement";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<FieldStatement> getValueType() {
		return FieldStatement.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		if(value == null)
			return null;
		Statement statement = value.getStatement();
		if(statement == null)
			return null;
		if(statement instanceof StringStatement)
			return "$S(" + statement.getText() + ")";
		if(statement instanceof VariableStatement)
			return "$V(" + typeToString(statement.getType()) + "," + statement.getText() + ")";
		if(statement instanceof ParameterStatement)
			return "$P(" + typeToString(statement.getType()) + "," + statement.getText() + ")";
		if(statement instanceof EquationStatement)
			return "$E(" + statement.getText() + ")";
		if(statement instanceof BufferStatement)
			return "$B(" + statement.getText() + ")";
		return null;
	}


	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		if(stringValue == null) {
			value = null;
			return;
		}
		
		stringValue = stringValue.trim();
		
		if(stringValue.length() < 5) {
			value = null;
			return;
		}
		
		if(stringValue.charAt(0) == '$') {
			if(	(stringValue.charAt(1) == 'S' || stringValue.charAt(1) == 's') && 
					stringValue.charAt(2) == '(' && 
					stringValue.charAt(stringValue.length() - 1) == ')') 
				{
					//This is a string statement.
					stringValue = stringValue.substring(3, stringValue.length()-1);
					
					StringStatement statement = new StringStatement(stringValue);
					value = new FieldStatement(statement);
					return;
				}
			if(	(stringValue.charAt(1) == 'V' || stringValue.charAt(1) == 'v') && 
					stringValue.charAt(2) == '(' && 
					stringValue.charAt(stringValue.length() - 1) == ')') 
				{
					//This is a variable statement.
					stringValue = StringUtil.eraseWhiteSpace(stringValue);
					stringValue = stringValue.substring(3, stringValue.length()-1);
					int indexComma = stringValue.indexOf(",");
					if(indexComma <= 0 || indexComma >= stringValue.length()-1) {
						value = null;
						return;
					}
					String type = stringValue.substring(0, indexComma);
					String fullStatement = stringValue.substring(indexComma+1);
					ObjType typeValue = typeValueOf(type);
					
					VariableStatement statement = new VariableStatement(typeValue, fullStatement);
					value = new FieldStatement(statement);
					return;
				}
			if((stringValue.charAt(1) == 'P' || stringValue.charAt(1) == 'p') && 
					stringValue.charAt(2) == '(' && 
					stringValue.charAt(stringValue.length() - 1) == ')') 
			{
				//This is a parameter statement.
				stringValue = StringUtil.eraseWhiteSpace(stringValue);
				stringValue = stringValue.substring(3, stringValue.length()-1);
				int indexComma = stringValue.indexOf(",");
				if(indexComma <= 0 || indexComma >= stringValue.length()-1) {
					value = null;
					return;
				}
				String type = stringValue.substring(0, indexComma);
				String fullStatement = stringValue.substring(indexComma+1);
				ObjType typeValue = typeValueOf(type);
				
				ParameterStatement statement = new ParameterStatement(typeValue, fullStatement);
				value = new FieldStatement(statement);
				return;
			}
			if(	(stringValue.charAt(1) == 'E' || stringValue.charAt(1) == 'e') && 
				stringValue.charAt(2) == '(' && 
				stringValue.charAt(stringValue.length() - 1) == ')') 
				{
					//This is a equation statement.
					stringValue = StringUtil.eraseWhiteSpace(stringValue);
					stringValue = stringValue.substring(3, stringValue.length()-1);
					EquationStatement statement = null;
					try {
						statement = EquationStatementParser.parse(stringValue, null);
					}
					catch(BaseException e) {
						value = null;
						return;
					}
					value = new FieldStatement(statement);
					return;
				}
			if(	(stringValue.charAt(1) == 'B' || stringValue.charAt(1) == 'b') && 
				stringValue.charAt(2) == '(' && 
				stringValue.charAt(stringValue.length() - 1) == ')') 
				{
					//This is a buffer statement.
					stringValue = stringValue.substring(3, stringValue.length()-1);
					BufferStatement statement = null;
					try {
						statement = BufferStatementParser.parse(stringValue, null);
					}
					catch(BaseException e) {
						value = null;
						return;
					}
					value = new FieldStatement(statement);
					return;
				}
		}//if(stringValue.charAt(0) == '$')

		//Does not match to any statement type.
		value = FieldStatement.Empty;
	}
	
	/**
	 * Returns the type id of the given source.
	 * @param source
	 * @return
	 */
	public static ObjType typeValueOf(String source) {
		return ObjType.valueOf(source);
	}

	/**
	 * Returns the string value of given type id.
	 * @param type
	 * @return
	 */
	public static String typeToString(ObjType type) {
		return type.name();
	}
}
